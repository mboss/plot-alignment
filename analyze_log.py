from pathlib import Path


def extract_log_line_type(line):
    """
    Extracts the log line type from a given log line.
    """
    parts = line.split("|")
    if len(parts) > 2:
        log_type_parts = parts[2].split(" - ")
        if len(log_type_parts) > 0:
            return log_type_parts[0].strip()
    return None


def analyze_log_file(log_file_path):
    """
    Reads a log file and counts the occurrences of each log line type.
    """
    log_line_counts = {}
    with Path(log_file_path).open() as file:
        for line in file:
            log_line_type = extract_log_line_type(line)
            if log_line_type:
                log_line_counts[log_line_type] = (
                    log_line_counts.get(log_line_type, 0) + 1
                )

    return dict(sorted(log_line_counts.items(), key=lambda item: item[1], reverse=True))


# Set the path to your log file here
log_file_path = "./logs/predict_2018_WW022.log"

# Analyze the log file
log_line_counts_sorted = analyze_log_file(log_file_path)

# Print the results
for log_line, count in log_line_counts_sorted.items():
    print(f"{log_line}: {count}")
