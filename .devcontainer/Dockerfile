FROM pytorch/pytorch:2.1.0-cuda12.1-cudnn8-devel as pytorch

ENV DEBIAN_FRONTEND=noninteractive
ENV CONDA_AUTO_ACTIVATE_BASE=false

ARG USERNAME=mboss
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends git curl libssl-dev zlib1g-dev \
    libbz2-dev libreadline-dev libffi-dev openssh-server liblzma-dev git-lfs \
    sudo libsm6 libxext6 libsqlite3-dev htop ffmpeg cargo locales \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

RUN sed -i '/de_CH.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG de_CH.UTF-8  
ENV LANGUAGE de_CH:en  
ENV LC_ALL de_CH.UTF-8   

USER $USERNAME
WORKDIR /home/$USERNAME

ENV PATH /home/${USERNAME}/.cargo/bin:$PATH
RUN cargo install --locked zellij

ENV PYENV_ROOT /home/${USERNAME}/.pyenv
ENV PATH $PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH
ENV PYTHON_VERSION=3.11.6

RUN set -ex \
    && curl https://pyenv.run | bash \
    && pyenv update \
    && eval "$(pyenv init --path)" \
    && pyenv install $PYTHON_VERSION \
    && pyenv global $PYTHON_VERSION \
    && pyenv rehash

ENV POETRY_HOME=/home/${USERNAME}/.poetry
ENV POETRY_CACHE_DIR=/home/${USERNAME}/.poetry/pypoetry
ENV PATH="$POETRY_HOME/bin:$PATH"

WORKDIR /workspaces/plot-alignment
COPY pyproject.toml poetry.lock ./

RUN curl -sSL https://install.python-poetry.org | python -
RUN poetry install --no-root --with style

COPY .devcontainer/bash_functions.sh /home/${USERNAME}/bash_functions.sh
RUN echo "source /home/${USERNAME}/bash_functions.sh" >> /home/${USERNAME}/.bashrc

ENV TERM xterm-256color
ENV SHELL /bin/bash
CMD ["bash", "-l"]