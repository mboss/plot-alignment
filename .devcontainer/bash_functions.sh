function wwc() {
    dir="$1"
    watch -n 1 "find \"$dir\" | wc -l"
}

function wls() {
    dir="$1"
    watch -n 1 "ls \"$dir\""
}