# File locations
- [FIP folder]() : ~/public/Evaluation/FIP/
    - [Image folder](): {year}/{experiment_number}/RGB1/
    
    - [Reference folder](): Analysis/{year}/{experiment_number}/RGB_Referencing/
        
        - [Camera calibration]():   RGB_calib.xml
        
        - [Plot real coordinates](): heights_spline_predicts.csv
        
        - [Plot heights](): heights_spline_predicts.csv
        
        - [Align references](): align_references/
            - If three files exist => alignment has been found
            - [Camera position](): FP{experiment_number}{plot_uid}\_RGB1_{date}_{time}_posXYZ.csv
            
            - [Camera orientation](): FP{experiment_number}{plot_uid}\_RGB1_{date}_{time}_rotMat.csv
            
            - [Polygons](): FP{experiment_number}{plot_uid}\_RGB1_{date}_{time}.geojson
                - Soil and canopy polygons for the plot
                - Canopy polygon created by offsetting soil polygon by canopy height
                - Includes camera position and orientation
                - Based on transforming the initial reference polygon
        
        - [Initial references](): initial_references/FP{experiment_number}{plot_uid}\_{date}_{time}.geojson
            - Initial polygon created by drone referencing
        
        - [Manual references](): manual_references/FP{experiment_number}{plot_uid}\_{date0}_{date1}.csv
            - Alignment points pair (4 & 4) created by Hiwi
            - Are never deleted
        
        - [Deadends](): reports/align_(deadend|largeoffset)\_FP{experiment_number}{plot_uid}\_{date}_{time}.txt
            - Deadends where process failed at

 