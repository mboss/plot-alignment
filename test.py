import warnings
from typing import TYPE_CHECKING, Any

import hydra
import torch
from hydra.utils import instantiate
from loguru import logger
from pytorch_lightning import seed_everything

if TYPE_CHECKING:
    from omegaconf import DictConfig


@hydra.main(config_path="conf", config_name="test", version_base=None)
def main(cfg: "DictConfig | dict[str, Any]") -> None:
    logger.remove(0)
    logger.add("logs/test.log", mode="w")

    seed_everything(cfg["seed"], workers=True)

    dm = instantiate(cfg["datamodule"])
    system = instantiate(cfg["system"])

    trainer = instantiate(cfg["trainer"])
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")

        trainer.test(system, datamodule=dm)


if __name__ == "__main__":
    torch.set_float32_matmul_precision("medium")

    main()
