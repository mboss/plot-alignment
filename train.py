import warnings
from pathlib import Path
from typing import Any

import hydra
import torch
from hydra.utils import instantiate
from loguru import logger
from omegaconf import DictConfig
from pytorch_lightning import seed_everything
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint


@hydra.main(config_path="conf", config_name="train", version_base=None)
def main(cfg: DictConfig | dict[str, Any]) -> None:
    logger.remove(0)
    logger.add("logs/train.log", mode="w")

    seed_everything(cfg["seed"], workers=True)

    if not Path(cfg["save_root"]).is_dir():
        Path(cfg["save_root"]).mkdir(parents=True)
    if not Path(cfg["save_root_train"]).is_dir():
        Path(cfg["save_root_train"]).mkdir(parents=True)
    if not Path(cfg["ckpt_folder"]).is_dir():
        Path(cfg["ckpt_folder"]).mkdir(parents=True)

    dm = instantiate(cfg["datamodule"])
    system = instantiate(cfg["system"])

    callbacks = [
        LearningRateMonitor(logging_interval="step"),
        ModelCheckpoint(
            dirpath=cfg["ckpt_folder"],
            every_n_train_steps=50,
            save_top_k=-1,
            save_last=True,
        ),
    ]
    trainer = instantiate(cfg["trainer"], callbacks=callbacks)

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")

        ckpt_path = None
        if cfg["use_ckpt"]:
            # TODO: select last ckpt automatically if "last"
            ckpt_path = str(Path(cfg["ckpt_folder"]) / cfg["ckpt_name"])

        trainer.fit(system, datamodule=dm, ckpt_path=ckpt_path)


if __name__ == "__main__":
    torch.set_float32_matmul_precision("medium")

    main()
