from typing import Any

import rawpy
import torch
from kornia.geometry import sample_is_valid_for_homography
from loguru import logger
from shapely import Point, Polygon, box, convex_hull
from torch import Tensor, vmap

from .point import calculate_homographies, transform_points_normalized
from .raw import postprocess_raw


# https://github.com/pytorch/pytorch/issues/59194
# https://ch.mathworks.com/matlabcentral/answers/16243-angle-between-two-vectors-in-3d
def angle_v2(a, b, dim=-1):
    a_norm = a.norm(dim=dim, keepdim=True)
    b_norm = b.norm(dim=dim, keepdim=True)
    return 2 * torch.atan2(
        (a * b_norm - a_norm * b).norm(dim=dim),
        (a * b_norm + a_norm * b).norm(dim=dim),
    )


def get_angles(corners: Tensor) -> float:
    vectors = corners.roll(-1, dims=1) - corners
    return torch.rad2deg(angle_v2(-vectors.roll(1, dims=1), vectors))


def check_angles(
    corners: Tensor,
    min_angle: float = 85.0,
    max_angle: float = 95.0,
    std_angle: float = 4.0,
) -> tuple[Tensor, Tensor]:
    is_batch = len(corners.shape) == 3
    if not is_batch:
        corners = corners[None]

    angles = get_angles(corners)
    correct_angles = ((angles >= min_angle) & (angles <= max_angle)).all(
        dim=1
    ) and angles.std(dim=1) <= std_angle

    if not is_batch:
        return correct_angles[0], angles[0]

    return correct_angles, angles


def check_if_readable_raw(filepath: str) -> bool:
    """Check if raw file is correct by checking if it contains the correct
    number of channels.

    Args:
        filepath (str): path to raw file

    Returns:
        bool: True if raw file is correct, False otherwise
    """
    try:
        with rawpy.imread(filepath) as raw:
            postprocess_raw(raw)
            return True
    except Exception:
        return False


def check_initial_reference_validity(sample: dict[str, Any]) -> bool:
    polygon = sample["polygon"]

    pdist = torch.pdist(polygon)[[0, 2, 3, 5]]
    if pdist.max() / pdist.min() > 1.75:
        logger.warning(
            f"Initial reference side difference too large: {pdist.max() / pdist.min()}"
        )
        return False

    correct_angles, angles = check_angles(
        polygon, min_angle=70.0, max_angle=110.0, std_angle=15.0
    )

    if not correct_angles:
        logger.warning(f"Initial reference angles not correct: {angles}")
        return False

    return True


def check_reference_validity(
    sample: dict[str, Any],
    image_size_original: Tensor | None = None,
    camera_matrix: Tensor | None = None,
) -> bool:
    image_size_original = (
        sample["image_size_original"]
        if image_size_original is None
        else image_size_original
    )
    H, W = (
        image_size_original.tolist()
        if isinstance(image_size_original, Tensor)
        else image_size_original
    )

    camera_matrix = sample["camera_matrix"] if camera_matrix is None else camera_matrix
    points0 = sample["points0"]
    points1 = sample["points1"]

    if (
        (points0 < 0).any()
        or (points0[..., 0] > W).any()
        or (points0[..., 1] > H).any()
        or (points1 < 0).any()
        or (points1[..., 0] > W).any()
        or (points1[..., 1] > H).any()
    ):
        return False

    # left up is from the image point of view has 0,0 at the top left corner
    lu0, ru0, rd0, ld0 = points0
    lu1, ru1, rd1, ld1 = points1

    (quadrant_lu, quadrant_ru, quadrant_rd, quadrant_ld) = create_quadrants(W, H)
    points0_in_quadrants = bool(
        quadrant_lu.contains(Point(lu0))
        and quadrant_ru.contains(Point(ru0))
        and quadrant_rd.contains(Point(rd0))
        and quadrant_ld.contains(Point(ld0))
    )
    if not points0_in_quadrants:
        return False

    points1_in_quadrants = bool(
        quadrant_lu.contains(Point(lu1))
        and quadrant_ru.contains(Point(ru1))
        and quadrant_rd.contains(Point(rd1))
        and quadrant_ld.contains(Point(ld1))
    )
    if not points1_in_quadrants:
        return False

    polygon0 = Polygon([lu0, ru0, rd0, ld0])
    if len(convex_hull(polygon0).exterior.coords) != 5:
        return False

    polygon1 = Polygon([lu1, ru1, rd1, ld1])
    if len(convex_hull(polygon1).exterior.coords) != 5:
        return False

    polygon_area_ratio = polygon0.area / polygon1.area
    if polygon_area_ratio < 0.5 or polygon_area_ratio > 2:
        return False

    points0 = sample["points0"][None].clone()
    points1 = sample["points1"][None].clone()
    if not sample_is_valid_for_homography(points0, points1).all():
        return False
    sample = calculate_homographies(sample, camera_matrix=camera_matrix)
    hom_0to1, hom_1to0 = sample["hom_0to1"][None], sample["hom_1to0"][None]
    del sample["hom_0to1"]
    del sample["hom_1to0"]

    if hom_0to1.isnan().any() or hom_1to0.isnan().any():
        return False

    points_w0 = transform_points_normalized(points0, hom_0to1, camera_matrix)
    if not points_w0.allclose(points1, rtol=1e-3):
        return False

    points_w1 = transform_points_normalized(points1, hom_1to0, camera_matrix)
    if not points_w1.allclose(points0, rtol=1e-3):
        return False

    return True


def create_quadrants(
    x_max: int, y_max: int
) -> tuple[Polygon, Polygon, Polygon, Polygon]:
    """Create quadrants from an image using box.

    Args:
        x_max (int): width of the image
        y_max (int): height of the image

    Returns:
        tuple[Polygon]: quadrants
    """
    x_mid = x_max // 2
    y_mid = y_max // 2

    return (
        box(0, 0, x_mid, y_mid),
        box(x_mid, 0, x_max, y_mid),
        box(x_mid, y_mid, x_max, y_max),
        box(0, y_mid, x_mid, y_max),
    )
