from typing import Any

import torch
from kornia.geometry import (
    denormalize_points_with_intrinsics,
    find_homography_dlt,
    normalize_points_with_intrinsics,
    transform_points,
)
from kornia.geometry.bbox import bbox_generator
from torch import Tensor
from torchvision.transforms.functional import get_image_size


def transform_points_normalized(
    points: Tensor, normalized_transform: Tensor, camera_matrix: Tensor
) -> Tensor:
    points_shape = points.shape

    points = points.reshape(-1, 2)
    camera_matrix = camera_matrix.repeat_interleave(
        len(points) // len(camera_matrix), dim=0
    )
    points = normalize_points_with_intrinsics(points, camera_matrix)

    points = points.reshape(points_shape)
    points = transform_points(normalized_transform, points)

    points = points.reshape(-1, 2)
    points = denormalize_points_with_intrinsics(points, camera_matrix)

    return points.reshape(points_shape)


def sort_corners(corners: Tensor) -> Tensor:
    """Sort corners to be top-left, top-right, bottom-right, bottom-left based on x, y.

    Args:
        corners (Tensor): Corners of a polygon (4, *).

    Returns:
        Tensor: Sorted corners.
    """
    corners_l, corners_r = corners[corners[:, 0].argsort()].split(2)
    corners_l = corners_l[corners_l[:, 1].argsort()]
    corners_r = corners_r[corners_r[:, 1].argsort()]
    return torch.stack([corners_l[0], corners_r[0], corners_r[1], corners_l[1]])


def get_corners(image: Tensor) -> Tensor:
    N = image.shape[0]
    W, H = get_image_size(image)

    x_start = torch.zeros(N, device=image.device)
    y_start = torch.zeros(N, device=image.device)
    x_end = torch.full((N,), W, dtype=torch.float, device=image.device)
    y_end = torch.full((N,), H, dtype=torch.float, device=image.device)

    return bbox_generator(x_start, y_start, x_end, y_end)


def calculate_homographies(
    sample: dict[str, Any], camera_matrix: Tensor | None = None
) -> dict[str, Any]:
    """Calculate homographies from points0 to points1 and vice versa.

    Args:
        sample (dict[str, Any]): sample dictionary with points0 and points1

    Returns:
        dict[str, Any]: sample dictionary with homographies
    """
    points0, points1 = sample["points0"], sample["points1"]
    is_batch = len(points0.shape) == 3
    camera_matrix = sample["camera_matrix"] if camera_matrix is None else camera_matrix

    if not is_batch:
        points0 = points0[None]
        points1 = points1[None]

    points0 = normalize_points_with_intrinsics(points0, camera_matrix)
    points1 = normalize_points_with_intrinsics(points1, camera_matrix)

    hom_0to1 = find_homography_dlt(points0, points1, solver="lu")
    if hom_0to1.isnan().any():
        hom_0to1 = find_homography_dlt(points0, points1, solver="svd")
    hom_1to0 = hom_0to1.inverse()

    if not is_batch:
        hom_0to1 = hom_0to1[0]
        hom_1to0 = hom_1to0[0]

    sample["hom_0to1"] = hom_0to1
    sample["hom_1to0"] = hom_1to0

    return sample
