"""Functional image transforms."""


import torch
from kornia.color import rgb_to_xyz, xyz_to_rgb
from torch import Tensor


def xyz_to_rgb_01(image: Tensor, max_: int = 2**16 - 1) -> Tensor:
    is_batch = len(image.shape) == 4

    if not is_batch:
        image = image[None]

    N, *_ = image.shape
    xyz_max = rgb_to_xyz(torch.ones((N, 3, 1, 1), device=image.device))[0, :, 0, 0]
    image = ((image / max_).permute(0, 2, 3, 1) * xyz_max).permute(0, 3, 1, 2)
    image = xyz_to_rgb(image).clamp(0, 1)

    if not is_batch:
        image = image[0]

    return image
