"""Functional transforms for raw images."""

from typing import TYPE_CHECKING, cast

import numpy as np
import numpy.typing as npt
import rawpy

if TYPE_CHECKING:
    from rawpy._rawpy import RawPy


def postprocess_raw(image_raw: "RawPy") -> npt.NDArray[np.uint16]:
    """Postprocess raw image using rawpy postprocess.

    Args:
        image (RawPy): raw image to postprocess

    Returns:
        npt.NDArray[np.uint16]: postprocessed image
    """
    image = cast(
        npt.NDArray[np.uint16],
        image_raw.postprocess(
            output_bps=16,
            output_color=rawpy.ColorSpace.XYZ,
            demosaic_algorithm=0,
            user_flip=0,
            no_auto_bright=True,
            use_auto_wb=False,
        ),
    )
    image_raw.close()

    return image
