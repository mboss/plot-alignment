"""Functional utility transforms."""


from typing import TYPE_CHECKING, cast

import numpy as np
import torch
from torch import Tensor
from torchvision.transforms.functional import to_tensor

if TYPE_CHECKING:
    import numpy.typing as npt


def to_tensor_f32(image: "npt.NDArray[np.float32 | np.uint16] | Tensor") -> Tensor:
    """To tensor and between (0, 1).

    Args:
        image (npt.NDArray[np.float32 | np.uint16]): numpy image to convert

    Returns:
        Tensor: tensor image
    """
    if not isinstance(image, Tensor):
        return cast(Tensor, to_tensor(image.astype(np.float32)))

    if image.dtype in (torch.float16, torch.float32, torch.float64):
        return image

    return image / 255.0
