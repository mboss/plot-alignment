import cv2
import numpy as np
import numpy.typing as npt
import plotly.graph_objects as go
import scipy
import torch
from kornia.geometry import (
    denormalize_points_with_intrinsics,
    get_closest_point_on_epipolar_line,
)
from kornia.geometry.plane import fit_plane
from kornia.geometry.vector import Vector3
from loguru import logger
from torch import Tensor
from torch.nn.functional import pdist

from plotalignment.transforms.functional import check_angles, sort_corners


def find_plot_corners_with_essential(
    points0: Tensor,
    points1: Tensor,
    batch: dict[str, Tensor],
    *,
    max_num_essential_matrix_iterations: int = 3,
    threshold_max_epipolar_distance_percentage: float,
    threshold_mean_epipolar_distance_percentage: float,
    threshold_min_image_angle: float,
    threshold_max_image_angle: float,
    threshold_std_image_angle: float,
    threshold_max_image_angle_diff_percentage: float,
    threshold_mean_image_angle_diff_percentage: float,
    threshold_std_image_angle_diff_percentage: float,
    threshold_triangulated_min_angle: float,
    threshold_triangulated_max_angle: float,
    threshold_triangulated_std_angle: float,
    threshold_max_triangulated_angle_diff_percentage: float,
    threshold_mean_triangulated_angle_diff_percentage: float,
    threshold_std_triangulated_angle_diff_percentage: float,
    threshold_max_side_length: float,
    threshold_min_side_length: float,
    threshold_max_diff_sides_percentage: float,
    threshold_mean_diff_sides_percentage: float,
    threshold_std_diff_sides_percentage: float,
    max_distance_cameras: float,
) -> tuple[Tensor, Tensor]:
    has_corners = "plot_corners" in batch
    has_polygon = "polygon" in batch

    plot_corners = torch.full(
        (points0.size(0), 4, 2), np.nan, dtype=torch.float64, device=points0.device
    )
    polygons = torch.full(
        (points0.size(0), 4, 2), np.nan, dtype=torch.float64, device=points0.device
    )
    homs_0to1 = torch.full(
        (points0.size(0), 3, 3), np.nan, dtype=torch.float64, device=points0.device
    )

    if not has_corners:
        batch["plot_corners"] = plot_corners
    if not has_polygon:
        batch["polygon"] = batch.get("points0", polygons)

    for i, (
        points_pred1,
        points_pred2,
        camera_matrix,
        plot_corners1,
        plot_corners_world,
    ) in enumerate(
        zip(
            points0,
            points1,
            batch["camera_matrix"],
            batch["polygon"],
            batch["plot_corners"],
            strict=True,
        )
    ):
        if points_pred1.shape[0] < 8 or points_pred2.shape[0] < 8:
            logger.warning("Less than 8 points.")
            continue

        points_pred_t1 = points_pred1.clone()
        points_pred_t2 = points_pred2.clone()

        # 1. Normalize point coordinates
        pred_undist1 = cv2.undistortPoints(
            points_pred_t1.numpy(force=True),
            camera_matrix.numpy(force=True),
            None,
        )[:, 0]

        pred_undist2 = cv2.undistortPoints(
            points_pred_t2.numpy(force=True),
            camera_matrix.numpy(force=True),
            None,
        )[:, 0]

        plot_corners_undist1 = cv2.undistortPoints(
            plot_corners1.numpy(force=True),
            camera_matrix.numpy(force=True),
            None,
        )[:, 0]

        plot_corners_undist_t1 = torch.as_tensor(
            plot_corners_undist1, device=camera_matrix.device
        )

        num_inliers = np.inf
        preds2 = []
        diffs_angles_percentages = []
        diffs_distances_sides = []
        distances_cameras = []
        for _ in range(max_num_essential_matrix_iterations):
            # 2. Calculate essential matrix
            E, inliers = cv2.findEssentialMat(
                points1=pred_undist1,
                points2=pred_undist2,
                method=cv2.LMEDS,
                prob=0.995,
                maxIters=1_000_000,
            )

            if (
                E is None
                or inliers is None
                or inliers.sum() < 8
                or inliers.sum() >= num_inliers
            ):
                break

            inliers = inliers.astype(bool)[:, 0]
            num_inliers = inliers.sum()
            pred_undist1 = pred_undist1[inliers]
            pred_undist2 = pred_undist2[inliers]

            # 3. Calculate homography
            hom_0to1, plot_corners_undist2 = warp_using_homography(
                plot_corners_undist1, pred_undist1, pred_undist2
            )
            if hom_0to1 is None or plot_corners_undist2 is None:
                continue

            # 4. Match homography to essential
            (
                plot_corners_epipolar2,
                plot_corners_denorm2,
                plot_corners_epipolar_denorm2,
            ) = match_homography_to_essential(
                plot_corners_undist_t1, E, plot_corners_undist2, camera_matrix
            )

            if not check_epipolar_distances(
                plot_corners_denorm2,
                plot_corners_epipolar_denorm2,
                threshold_max_epipolar_distance_percentage=threshold_max_epipolar_distance_percentage,
                threshold_mean_epipolar_distance_percentage=threshold_mean_epipolar_distance_percentage,
            ):
                continue

            # 5.1 Check if reference corners are consistent
            correct_angles, angles_abs1 = check_angles(
                plot_corners_undist_t1,
                min_angle=threshold_min_image_angle,
                max_angle=threshold_max_image_angle,
                std_angle=threshold_std_image_angle,
            )
            if not correct_angles:
                logger.warning(f"Angles of reference incorrect: {angles_abs1}")
                continue

            # 5.2 Check if plot corner angles are consistent
            correct_angles, angles_abs2 = check_angles(
                plot_corners_epipolar2,
                min_angle=threshold_min_image_angle,
                max_angle=threshold_max_image_angle,
                std_angle=threshold_std_image_angle,
            )
            if not correct_angles:
                logger.warning(f"Angles incorrect: {angles_abs2}")
                continue

            # 5.3 Check if angle differences are consistent
            if not check_diff_angles(
                angles_abs1,
                angles_abs2,
                threshold_max_percentage=threshold_max_image_angle_diff_percentage,
                threshold_mean_percentage=threshold_mean_image_angle_diff_percentage,
                threshold_std_percentage=threshold_std_image_angle_diff_percentage,
            ):
                continue

            # 6. Triangulate corners
            (
                points_triangulated,
                tvec_triangulated,
                all_corners_inliers,
            ) = triangulate_corners(
                pred_undist1,
                pred_undist2,
                plot_corners_undist1,
                plot_corners_epipolar2,
                E,
            )
            if not all_corners_inliers:
                continue

            # 8. Check side distances of triangulated points
            (
                diff_distances_sides_percentage,
                correct_side_distances,
                scale_factor,
            ) = check_side_distances(
                plot_corners_world,
                points_triangulated,
                threshold_min_side_length=threshold_min_side_length,
                threshold_max_side_length=threshold_max_side_length,
                threshold_max_diff_sides_percentage=threshold_max_diff_sides_percentage,
                threshold_mean_diff_sides_percentage=threshold_mean_diff_sides_percentage,
                threshold_std_diff_sides_percentage=threshold_std_diff_sides_percentage,
            )
            if not correct_side_distances:
                continue

            # 9. Check side distances of triangulated points
            # diff_angle_percentages, correct_angles = check_triangulated_angles(
            #     plot_corners_world,
            #     points_triangulated,
            #     threshold_min_angle=threshold_triangulated_min_angle,
            #     threshold_max_angle=threshold_triangulated_max_angle,
            #     threshold_std_angle=threshold_triangulated_std_angle,
            #     threshold_max_angle_diff_percentage=threshold_max_triangulated_angle_diff_percentage,
            #     threshold_mean_angle_diff_percentage=threshold_mean_triangulated_angle_diff_percentage,
            #     threshold_std_angle_diff_percentage=threshold_std_triangulated_angle_diff_percentage,
            # )
            # if not correct_angles:
            #     continue

            distance_cameras = (scale_factor * tvec_triangulated).norm()
            if distance_cameras > max_distance_cameras:
                logger.warning(
                    f"Distance between cameras too large: {distance_cameras}"
                )
                continue

            preds2.append(plot_corners_epipolar_denorm2)
            distances_cameras.append(distance_cameras)
            # diffs_angles_percentages.append(diff_angle_percentages)
            diffs_distances_sides.append(diff_distances_sides_percentage)

        if len(preds2) > 0:
            # diffs_angles_max = torch.stack(diffs_angles_percentages).max(dim=1)[0]
            # diffs_angles_mean = torch.stack(diffs_angles_percentages).mean(dim=1)
            # diffs_angles_std = torch.stack(diffs_angles_percentages).std(dim=1)
            diffs_distances_sides_max = torch.stack(diffs_distances_sides).max(dim=1)[0]
            diffs_distances_sides_mean = torch.stack(diffs_distances_sides).mean(dim=1)
            diffs_distances_sides_std = torch.stack(diffs_distances_sides).std(dim=1)
            distances_cameras = torch.as_tensor(distances_cameras)

            min_indices = torch.stack(
                (
                    # diffs_angles_max,
                    # diffs_angles_mean,
                    # diffs_angles_std,
                    diffs_distances_sides_max,
                    diffs_distances_sides_mean,
                    diffs_distances_sides_std,
                    distances_cameras,
                )
            ).argmin(dim=1)
            min_counts = torch.bincount(min_indices, minlength=len(min_indices))
            best_index = torch.argmax(min_counts).item()

            plot_corners[i] = preds2[best_index].detach().clone()

    return plot_corners, homs_0to1


def warp_using_homography(
    plot_corners_undist1: npt.NDArray[np.float64],
    pred_undist1: npt.NDArray[np.float64],
    pred_undist2: npt.NDArray[np.float64],
) -> tuple[npt.NDArray[np.float64], npt.NDArray[np.float64]] | tuple[None, None]:
    hom_0to1, inliers = cv2.findHomography(
        srcPoints=pred_undist1,
        dstPoints=pred_undist2,
        method=cv2.LMEDS,
        confidence=0.995,
        maxIters=1_000_000,
    )

    if hom_0to1 is None or inliers is None:
        logger.warning("No homography found.")
        return None, None

    try:
        scipy.linalg.inv(hom_0to1)
    except (np.linalg.LinAlgError, ValueError):
        logger.warning("Homography not invertible.")
        return None, None

    plot_corners_undist2 = cv2.perspectiveTransform(
        plot_corners_undist1[:, None], hom_0to1
    )[:, 0]

    plot_corners_transformed2 = cv2.perspectiveTransform(
        plot_corners_undist2[:, None], scipy.linalg.inv(hom_0to1)
    )[:, 0]

    if not np.allclose(plot_corners_undist1, plot_corners_transformed2):
        logger.warning("Warped points using homography not close.")
        return None, None

    return hom_0to1, plot_corners_undist2


def match_homography_to_essential(
    plot_corners_undist_t1: Tensor,
    E: npt.NDArray[np.float64],
    plot_corners_undist2: npt.NDArray[np.float64],
    camera_matrix: Tensor,
) -> tuple[Tensor, Tensor, Tensor]:
    plot_corners_undist_t2 = torch.as_tensor(
        plot_corners_undist2, dtype=torch.float64, device=camera_matrix.device
    )
    E_t = torch.as_tensor(E, dtype=torch.float64, device=camera_matrix.device)

    plot_corners_epipolar2 = get_closest_point_on_epipolar_line(
        plot_corners_undist_t1[None], plot_corners_undist_t2[None], E_t[None]
    )[0]

    plot_corners_denorm2 = denormalize_points_with_intrinsics(
        plot_corners_undist_t2[None], camera_matrix[None]
    )[0]
    plot_corners_epipolar_denorm2 = denormalize_points_with_intrinsics(
        plot_corners_epipolar2[None], camera_matrix[None]
    )[0]

    return plot_corners_epipolar2, plot_corners_denorm2, plot_corners_epipolar_denorm2


def check_epipolar_distances(
    plot_corners_denorm2: Tensor,
    plot_corners_epipolar_denorm2: Tensor,
    *,
    threshold_max_epipolar_distance_percentage: float,
    threshold_mean_epipolar_distance_percentage: float,
) -> bool:
    epipolar_distances = (plot_corners_denorm2 - plot_corners_epipolar_denorm2).norm(
        dim=1
    )
    rectangle_long_length = plot_corners_denorm2[0].dist(plot_corners_denorm2[1])
    epipolar_distances_percentage = epipolar_distances / rectangle_long_length * 100

    if epipolar_distances_percentage.max() > threshold_max_epipolar_distance_percentage:
        logger.warning(
            f"Max epipolar distance too large: {epipolar_distances_percentage.max():.4f}%."
        )
        return False

    if (
        epipolar_distances_percentage.mean()
        > threshold_mean_epipolar_distance_percentage
    ):
        logger.warning(
            f"Mean epipolar distance too large: {epipolar_distances_percentage.mean():.4f}%."
        )
        return False

    return True


def triangulate_corners(
    pred_undist1: npt.NDArray[np.float64],
    pred_undist2: npt.NDArray[np.float64],
    plot_corners_undist1: npt.NDArray[np.float64],
    plot_corners_epipolar2: Tensor,
    E: npt.NDArray[np.float64],
) -> tuple[Tensor | None, Tensor | None, bool]:
    pred_undist1 = np.concatenate([plot_corners_undist1, pred_undist1], axis=0)
    pred_undist2 = np.concatenate(
        [plot_corners_epipolar2.cpu().numpy(), pred_undist2], axis=0
    )

    points_triangulated, tvec, all_corners_inliers = triangulate_points_with_essential(
        E, pred_undist1, pred_undist2
    )
    if not all_corners_inliers:
        return None, None, all_corners_inliers

    points_triangulated_t = torch.as_tensor(
        points_triangulated, device=plot_corners_epipolar2.device
    )
    tvec_t = torch.as_tensor(tvec, device=plot_corners_epipolar2.device)

    return points_triangulated_t, tvec_t, all_corners_inliers


def check_triangulated_angles(
    plot_corners_world: Tensor,
    points_triangulated: Tensor,
    *,
    threshold_min_angle: float,
    threshold_max_angle: float,
    threshold_std_angle: float,
    threshold_max_angle_diff_percentage: float,
    threshold_mean_angle_diff_percentage: float,
    threshold_std_angle_diff_percentage: float,
) -> tuple[Tensor, bool]:
    angle_correct, angle_abs = check_angles(
        points_triangulated[:4, :3],
        min_angle=threshold_min_angle,
        max_angle=threshold_max_angle,
        std_angle=threshold_std_angle,
    )

    angle_correct_world, angle_abs_world = check_angles(
        plot_corners_world[..., :3],
        min_angle=threshold_min_angle,
        max_angle=threshold_max_angle,
        std_angle=threshold_std_angle,
    )

    angle_diff = (angle_abs_world - angle_abs).abs()
    angle_diff_percentage = angle_diff / angle_abs_world * 100

    if not angle_correct:
        logger.warning(f"Triangulated angles incorrect: {angle_abs}")
        return angle_diff_percentage, False
    if not angle_correct_world:
        logger.warning(f"World angles incorrect: {angle_abs_world}")
        return angle_diff_percentage, False

    if angle_diff_percentage.max() > threshold_max_angle_diff_percentage:
        logger.warning(
            f"Max angle difference too large: {angle_diff_percentage.max():.4f}%."
        )
        return angle_diff_percentage, False
    if angle_diff_percentage.mean() > threshold_mean_angle_diff_percentage:
        logger.warning(
            f"Mean angle difference too large: {angle_diff_percentage.mean():.4f}%."
        )
        return angle_diff_percentage, False
    if angle_diff_percentage.std() > threshold_std_angle_diff_percentage:
        logger.warning(
            f"Std angle difference too large: {angle_diff_percentage.std():.4f}%."
        )
        return angle_diff_percentage, False

    return angle_diff_percentage, True


def check_side_distances(
    plot_corners_world: Tensor,
    points_triangulated: Tensor,
    *,
    threshold_min_side_length: float,
    threshold_max_side_length: float,
    threshold_max_diff_sides_percentage: float,
    threshold_mean_diff_sides_percentage: float,
    threshold_std_diff_sides_percentage: float,
) -> tuple[Tensor, bool]:
    (
        diff_distances_sides_percentage,
        correct_side_lengths,
        scale_factor,
    ) = get_diff_side_distances(
        plot_corners_world,
        points_triangulated,
        threshold_min_side_length=threshold_min_side_length,
        threshold_max_side_length=threshold_max_side_length,
    )

    if not correct_side_lengths:
        return diff_distances_sides_percentage, False, scale_factor

    if diff_distances_sides_percentage.max() > threshold_max_diff_sides_percentage:
        logger.warning(
            f"Max side distance differences too large: {diff_distances_sides_percentage.max():.4f}%."
        )
        return diff_distances_sides_percentage, False, scale_factor
    if diff_distances_sides_percentage.mean() > threshold_mean_diff_sides_percentage:
        logger.warning(
            f"Mean side distance differences too large: {diff_distances_sides_percentage.mean():.4f}%."
        )
        return diff_distances_sides_percentage, False, scale_factor
    if diff_distances_sides_percentage.std() > threshold_std_diff_sides_percentage:
        logger.warning(
            f"Std side distance differences too large: {diff_distances_sides_percentage.std():.4f}%."
        )
        return diff_distances_sides_percentage, False, scale_factor

    return diff_distances_sides_percentage, True, scale_factor


def get_diff_side_distances(
    plot_corners_world: Tensor,
    points_triangulated: Tensor,
    *,
    threshold_min_side_length: float,
    threshold_max_side_length: float,
) -> tuple[Tensor, bool]:
    distances_world = pdist(plot_corners_world)[[0, 3, 5, 2]]
    distances_triangulated = pdist(points_triangulated[:4])[[0, 3, 5, 2]]
    scale_factor = (distances_world / distances_triangulated).mean()

    distances_triangulated_scaled = distances_triangulated * scale_factor
    diff_distances_sides = (distances_world - distances_triangulated_scaled).abs()
    diff_distances_sides_percentage = diff_distances_sides / distances_world * 100

    if distances_world.max() > threshold_max_side_length:
        logger.warning(f"Max world side length too large: {distances_world.max():.4f}.")
        return diff_distances_sides_percentage, False, scale_factor
    if distances_world.min() < threshold_min_side_length:
        logger.warning(f"Min world side length too small: {distances_world.min():.4f}.")
        return diff_distances_sides_percentage, False, scale_factor
    if distances_triangulated_scaled.max() > threshold_max_side_length:
        logger.warning(
            f"Max triangulated side length too large: {distances_triangulated_scaled.max():.4f}."
        )
        return diff_distances_sides_percentage, False, scale_factor
    if distances_triangulated_scaled.min() < threshold_min_side_length:
        logger.warning(
            f"Min triangulated side length too small: {distances_triangulated_scaled.min():.4f}."
        )
        return diff_distances_sides_percentage, False, scale_factor

    return diff_distances_sides_percentage, True, scale_factor


def check_diff_angles(
    angles_abs1: Tensor,
    angles_abs2: Tensor,
    threshold_max_percentage: float,
    threshold_mean_percentage: float,
    threshold_std_percentage: float,
) -> bool:
    angles_diff = (angles_abs1 - angles_abs2).abs()
    angles_diff_percentage = angles_diff / angles_abs1 * 100

    angles_diff_max_percentage = angles_diff_percentage.max()
    angles_diff_mean_percentage = angles_diff_percentage.mean()
    angles_diff_std_percentage = angles_diff_percentage.std()

    if angles_diff_max_percentage > threshold_max_percentage:
        logger.warning(
            f"Max angle difference too large: {angles_diff_max_percentage:.4f}%."
        )
        return False
    if angles_diff_mean_percentage > threshold_mean_percentage:
        logger.warning(
            f"Mean angle difference too large: {angles_diff_mean_percentage:.4f}%."
        )
        return False
    if angles_diff_std_percentage > threshold_std_percentage:
        logger.warning(
            f"Std angle difference too large: {angles_diff_std_percentage:.4f}%."
        )
        return False

    return True


def triangulate_points_with_essential(
    E: npt.NDArray[np.float64],
    pred_undist1: npt.NDArray[np.float64],
    pred_undist2: npt.NDArray[np.float64],
) -> tuple[npt.NDArray[np.float64] | None, npt.NDArray[np.float64] | None, bool]:
    # _, R, t, inliers = cv2.recoverPose(E, pred_undist1, pred_undist2)
    # P = np.column_stack([R, t])

    # inliers = inliers.astype(bool)[:, 0]
    # all_corners_inliers = inliers[:4].all()

    # if not all_corners_inliers:
    #     pred_undist1 = pred_undist1[:4]
    #     pred_undist2 = pred_undist2[:4]

    # _, R, t, inliers = cv2.recoverPose(E, pred_undist1, pred_undist2)
    # P = np.column_stack([R, t])

    # inliers = inliers.astype(bool)[:, 0]
    # all_corners_inliers = inliers[:4].all()

    # if not all_corners_inliers:
    #     logger.warning("Not all triangulated corners are inliers.")
    #     return None, None, all_corners_inliers

    # points_triangulated = cv2.triangulatePoints(
    #     projMatr1=np.eye(3, 4),
    #     projMatr2=P,
    #     projPoints1=pred_undist1.T,
    #     projPoints2=pred_undist2.T,
    # ).T
    # points_triangulated = cv2.convertPointsFromHomogeneous(points_triangulated)[:, 0]

    # return points_triangulated, t, all_corners_inliers

    pred_undist1 = pred_undist1[:4]
    pred_undist2 = pred_undist2[:4]

    R1, R2, t = cv2.decomposeEssentialMat(E)

    Ps = [
        np.column_stack([R1, t]),
        np.column_stack([R2, t]),
        np.column_stack([R1, -t]),
        np.column_stack([R2, -t]),
    ]
    points_triangulated_l = []
    nums_infront = []

    for P in Ps:
        points_triangulated = cv2.triangulatePoints(
            projMatr1=np.eye(3, 4),
            projMatr2=P,
            projPoints1=pred_undist1.T,
            projPoints2=pred_undist2.T,
        ).T
        points_triangulated = cv2.convertPointsFromHomogeneous(points_triangulated)[
            :, 0
        ]

        if (
            sort_corners(torch.as_tensor(points_triangulated)).numpy()
            != points_triangulated
        ).any():
            continue

        # cheirality check
        num_infront = (P[:3, :3][2] @ (points_triangulated.T - t) > 0).sum()
        nums_infront.append(num_infront)
        points_triangulated_l.append(points_triangulated)

    if len(points_triangulated_l) == 0:
        return None, None, False

    best_index = np.argmax(nums_infront)
    points_triangulated = points_triangulated_l[best_index]

    return points_triangulated, t, True

    if False:
        for t in points_triangulated_l:
            plot_triangulated(t)

    # if points_triangulated1[0, 0] > 0:
    #     points_triangulated1 = -points_triangulated1
    #     t = -t

    # if points_triangulated2[0, 0] > 0:
    #     points_triangulated2 = -points_triangulated2
    #     t = -t

    # points_triangulated1 = points_triangulated1 / points_triangulated1[:4, 2].mean()
    # points_triangulated2 = points_triangulated2 / points_triangulated2[:4, 2].mean()

    # if points_triangulated2[:4, 2].mean() < 0:
    #     return points_triangulated1
    # elif points_triangulated1[:4, 2].mean() < 0:
    #     return points_triangulated2
    # elif points_triangulated1.var() <= points_triangulated2.var():
    #     return points_triangulated1

    # return points_triangulated2


def correct_triangulated_points(
    plot_corners_world: Tensor, points_triangulated: Tensor
) -> tuple[Tensor, Tensor]:
    plot_corners_world_points = Vector3(plot_corners_world)
    points_triangulated_points = Vector3(points_triangulated)

    plot_corners_world_points = plot_corners_world_points - plot_corners_world_points[0]
    points_triangulated_points = (
        points_triangulated_points - points_triangulated_points[0]
    )

    plane_world = fit_plane(plot_corners_world_points)
    plane_triangulated = fit_plane(points_triangulated_points)

    v1_world = (
        plot_corners_world_points[1] - plot_corners_world_points[0]
    ).normalized()
    v2_world = (
        plot_corners_world_points[3] - plot_corners_world_points[0]
    ).normalized()
    normal_world = plane_world.normal

    if normal_world.z < 0:
        normal_world = -normal_world
    vector_basis = torch.column_stack([v1_world, v2_world, normal_world]).to_dense()

    plot_corners_world_transformed = torch.linalg.solve(
        vector_basis, plot_corners_world_points.to_dense().T
    ).T

    v1_triangulated = (
        points_triangulated_points[1] - points_triangulated_points[0]
    ).normalized()
    v2_triangulated = (
        points_triangulated_points[3] - points_triangulated_points[0]
    ).normalized()
    normal_triangulated = plane_triangulated.normal
    if normal_triangulated.z < 0:
        normal_triangulated = -normal_triangulated
    vector_basis = torch.column_stack(
        [v1_triangulated, v2_triangulated, normal_triangulated]
    ).to_dense()

    points_triangulated_transformed = torch.linalg.solve(
        vector_basis, points_triangulated_points.to_dense().T
    ).T

    # Scale

    distances_world = pdist(plot_corners_world_transformed[:, :3])[[0, 2]]
    distances_triangulated = pdist(points_triangulated_transformed[:4, :3])[[0, 2]]
    scale_factors = distances_world / distances_triangulated
    points_triangulated_scaled = points_triangulated_transformed * scale_factors.mean()

    return plot_corners_world_transformed, points_triangulated_scaled


def plot_2d_pred(points0, points1):
    scatter_plot_corners0 = go.Scatter(
        x=points0[:4, 0],
        y=points0[:4, 1],
        mode="markers",
        text=["0", "1", "2", "3"],
        marker={"color": "green"},
    )
    scatter_plot_corners1 = go.Scatter(
        x=points1[:4, 0],
        y=points1[:4, 1],
        mode="markers",
        text=["0", "1", "2", "3"],
        marker={"color": "red"},
    )
    scatter_pred_undist0 = go.Scatter(
        x=points0[4:, 0],
        y=points0[4:, 1],
        mode="markers",
        text=["0", "1", "2", "3"],
        marker={"color": "blue"},
    )
    scatter_pred_undist1 = go.Scatter(
        x=points1[4:, 0],
        y=points1[4:, 1],
        mode="markers",
        text=["0", "1", "2", "3"],
        marker={"color": "orange"},
    )
    fig = go.Figure(
        [
            scatter_pred_undist0,
            scatter_pred_undist1,
            scatter_plot_corners0,
            scatter_plot_corners1,
        ]
    )
    fig.show()


def plot_corners_3d(points):
    scatter_plot_corners = go.Scatter3d(
        x=points[:, 0],
        y=points[:, 1],
        z=points[:, 2],
        mode="markers",
        marker={"color": "green"},
        text=["0", "1", "2", "3"],
    )
    fig = go.Figure([scatter_plot_corners])
    fig.show()


def plot_triangulated(points):
    scatter_points_triangulated_3d = go.Scatter3d(
        x=points[:, 0],
        y=points[:, 1],
        z=points[:, 2],
        mode="markers",
        marker={"color": "red", "size": 5},
    )
    scatter_points_corners = go.Scatter3d(
        x=points[:4, 0],
        y=points[:4, 1],
        z=points[:4, 2],
        mode="markers",
        text=["0", "1", "2", "3"],
        marker={"color": "blue", "size": 5},
    )
    fig = go.Figure([scatter_points_triangulated_3d, scatter_points_corners])
    fig.show()

    scatter_points_triangulated = go.Scatter(
        x=points[:, 0], y=points[:, 1], mode="markers", marker={"color": "blue"}
    )
    scatter_corners_triangulated = go.Scatter(
        x=points[:4, 0],
        y=points[:4, 1],
        mode="markers",
        marker={"color": "green"},
        text=["0", "1", "2", "3"],
    )

    fig = go.Figure([scatter_points_triangulated, scatter_corners_triangulated])
    fig.show()


def plot_corrected(plot_corners, plot_corners_triangulated):
    scatter_plot_corners_corrected = go.Scatter3d(
        x=plot_corners[:, 0].cpu().numpy(),
        y=plot_corners[:, 1].cpu().numpy(),
        z=plot_corners[:, 2].cpu().numpy(),
        mode="markers",
        text=["0", "1", "2", "3"],
        marker={"color": "green"},
    )
    scatter_points_corrected = go.Scatter3d(
        x=plot_corners_triangulated[:4, 0].cpu().numpy(),
        y=plot_corners_triangulated[:4, 1].cpu().numpy(),
        z=plot_corners_triangulated[:4, 2].cpu().numpy(),
        mode="markers",
        text=["0", "1", "2", "3"],
        marker={"color": "red"},
    )
    fig = go.Figure([scatter_plot_corners_corrected, scatter_points_corrected])
    fig.show()
