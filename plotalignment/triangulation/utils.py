import torch
from torch import Tensor


def b_ids_to_batch(b_ids: Tensor, data: Tensor) -> tuple[Tensor, Tensor]:
    """Transforms a batch of data to a nested tensor given a tensor of batch ids.

    Args:
        b_ids (Tensor): batch ids
        data (Tensor): batch data to split into nested tensor

    Returns:
        Tensor: nested batch tensor
    """
    b_ids, b_ids_index = b_ids.sort(stable=True)
    data = data[b_ids_index]
    split_sizes = b_ids.unique_consecutive(return_counts=True)[1].tolist()

    return (
        torch.nested.as_nested_tensor(
            list(data.split(split_sizes)), device=data.device
        ),
        b_ids.unique(),
    )
