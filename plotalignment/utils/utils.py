"""Utilities."""

from collections import deque
from collections.abc import Callable, Iterable, Iterator, Sequence
from datetime import datetime
from pathlib import Path
from typing import Any, TypeVar, cast

import dill as pickle
from torch import Tensor
from tqdm import tqdm

T = TypeVar("T")

pickle.extend(use_dill=False)


def keys_are_equal(sample: dict[str, Any], key0: str, key1: str) -> bool:
    return cast(bool, sample[key0] == sample[key1])


def keys_are_unequal(sample: dict[str, Any], key0: str, key1: str) -> bool:
    return cast(bool, sample[key0] != sample[key1])


def to_tuple(x: T | tuple[T, ...], length: int = 2) -> tuple[T, ...]:
    """Converts a float or a tuple of floats to a tuple of floats."""
    if isinstance(x, Sequence):
        return cast(tuple[T, T], tuple(x))

    return (x,) * length


def tuple_to_dict(x: tuple[T, T]) -> dict[T, T]:
    return dict([x])


def combine_tuple_dict_value(x: tuple[dict[str, Any], Any], key: str) -> dict[str, Any]:
    dict_ = x[0]
    dict_[key] = x[1]
    return dict_


def consume(iterator: Iterator[Any], progress: bool = False) -> None:
    if progress:
        iterator = tqdm(iterator)
    deque(iterator, maxlen=0)


def add_dict_entry(dict_: dict[str, Any], key: str, value: Any) -> dict[str, Any]:
    dict_[key] = value
    return dict_


def remove_dict_entry(dict_: dict[str, Any], key: str) -> dict[str, Any]:
    if key in dict_:
        del dict_[key]
    return dict_


def remove_dict_entries(dict_: dict[str, Any], keys: list[str]) -> dict[str, Any]:
    for key in keys:
        if key in dict_:
            del dict_[key]
    return dict_


def combine_dicts(dict0: dict[str, Any], dict1: dict[str, Any]) -> dict[str, Any]:
    return dict0 | dict1


def dict_to_batch(data: dict[str, Any]) -> dict[str, Any]:
    return {key: to_batch(value) for key, value in data.items()}


def dict_from_batch(dict_: dict[str, Any]) -> dict[str, Any]:
    for key, value in dict_.items():
        if isinstance(value, list):
            dict_[key] = value[0]
        if isinstance(value, Tensor):
            dict_[key] = value.squeeze(0)

    return dict_


def to_pickle(sample):
    return [pickle.dumps(sample)]


def to_batch(data: Any) -> Any:
    if isinstance(data, Tensor):
        return data[None]

    return [data]


# TODO: Move somewhere more logical
def create_homography_path(sample: dict[str, Any], path: Path, index: int) -> str:
    date0 = (
        datetime.strptime(sample[f"date{index}"], "%Y-%m-%d").date().strftime("%Y%m%d")
    )
    date1 = (
        datetime.strptime(sample[f"date{1 - index}"], "%Y-%m-%d")
        .date()
        .strftime("%Y%m%d")
    )

    filename = (
        f"FP{sample['yearly_experiment_number']}{sample['plot_uid']}_{date0}_{date1}"
    )

    return str(path / filename)


def read_text(path: str) -> str:
    return Path(path).read_text()


def get_subdict(dict_: dict[str, Any], keys: list[str]) -> dict[str, Any]:
    return {key: dict_[key] for key in keys}


def call_if_condition(
    sample: dict[str, Any],
    condition: Callable[[dict[str, Any]], bool],
    function: Callable[[dict[str, Any]], dict[str, Any]],
) -> dict[str, Any]:
    if condition(sample):
        return function(sample)

    return sample


def call_if_not_condition(
    sample: dict[str, Any],
    condition: Callable[[dict[str, Any]], bool],
    function: Callable[[dict[str, Any]], dict[str, Any]],
) -> dict[str, Any]:
    if not condition(sample):
        return function(sample)

    return sample


def apply_transforms(value: Any, transforms: Iterable[Callable[[Any], Any]]) -> Any:
    """Process a single sample using the given load function and transforms.

    Args:
        data (str): data to process
        transforms (Iterable[Callable[[Any], Any]] | None): transforms to apply

    Returns:
        Tensor: processed sample
    """
    for transform in transforms:
        value = transform(value)

    return value


def merge_dicts(
    *dicts: dict[Any, Any],
    suffixes: str | tuple[str, str] = ("0", "1"),
    keys_to_rename: Sequence[str] = (),
) -> dict[Any, Any]:
    """Combine two dictionaries taking the keys of both dictionaries and renaming
    the keys present in both dictionaries that have different values.

    Args:
        dicts (tuple[dict[Any, Any], dict[Any, Any]]): two dictionaries to combine
        suffixes (str | tuple[str, str], optional): suffixes to append to the keys
            (Defaults to ("0", "1"))

    Returns:
        dict[Any, Any]: combined dictionary
    """
    dict0, dict1 = dicts
    suffix0, suffix1 = to_tuple(suffixes)

    items0, items1 = dict0.items(), dict1.items()
    keys0, keys1 = dict0.keys(), dict1.keys()

    items0 = {k: v for k, v in items0 if type(v) != Tensor}.items()
    items1 = {k: v for k, v in items1 if type(v) != Tensor}.items()

    items_same = dict(items0 & items1)
    items0_only = {k: dict0[k] for k in (keys0 - keys1)}
    items1_only = {k: dict1[k] for k in (keys1 - keys0)}

    keys_to_rename = ((keys0 & keys1) - items_same.keys()) | set(keys_to_rename)
    items0_renamed = {f"{k}{suffix0}": dict0[k] for k in keys_to_rename}
    items1_renamed = {f"{k}{suffix1}": dict1[k] for k in keys_to_rename}

    return items_same | items0_only | items1_only | items0_renamed | items1_renamed
