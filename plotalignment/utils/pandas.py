import re
from collections.abc import Iterable
from pathlib import Path

import pandas as pd
from fastcore.basics import tuplify
from loguru import logger
from torch.utils.data.datapipes.utils.common import get_file_pathnames_from_root


def read_filenames_pd(
    root: str | Path | Iterable[str] | Iterable[Path],
    masks: str | list[str],
    recursive: bool = True,
) -> pd.Series:
    root_t = tuplify(root)

    filenames: list[str] = []
    for root_path in root_t:
        if not Path(root_path).is_dir():
            logger.warning(f"Path {root_path} is not a directory")
            continue

        filenames.extend(
            get_file_pathnames_from_root(
                root=root_path, masks=masks, recursive=recursive
            )
        )
    return pd.Series(filenames)


def read_filenames_regex_pd(
    root: str | Path | Iterable[str] | Iterable[Path],
    masks: str | list[str],
    regex: str,
    path_column_name: str | None = None,
    recursive: bool = True,
) -> pd.DataFrame:
    ser = read_filenames_pd(root, masks, recursive)
    df = ser.str.extract(regex, flags=re.VERBOSE)

    if path_column_name is not None:
        df = ser.to_frame(path_column_name).join(df)

    df = df.dropna()

    return df


def filter_by_df(
    df: pd.DataFrame,
    df_filter: pd.DataFrame,
    on: list[str] | None = None,
    left_on: list[str] | None = None,
    right_on: list[str] | None = None,
) -> pd.DataFrame:
    if df_filter.empty:
        return df

    if on is not None:
        left_on = on
        right_on = on

    i1 = df.set_index(left_on).index
    i2 = df_filter.set_index(right_on).index
    return df[~i1.isin(i2)]
