from typing import Any

import cv2
import kornia as K
import matplotlib as mpl
import matplotlib.pyplot as plt
import torch
from kornia.utils import draw_convex_polygon, tensor_to_image
from PIL import Image
from torch import Tensor
from torchvision.transforms.functional import pil_to_tensor

from loftr.loftr.utils import make_matching_figure
from plotalignment.triangulation import b_ids_to_batch


def visualize_matches(
    batch: dict[str, Any], image_t0: Tensor, image_t1: Tensor, index: int = 0
) -> dict[str, Any]:
    image_t0, image_t1 = image_t0.detach().clone(), image_t1.detach().clone()
    m_bids = batch["m_bids"]
    points_f0 = (
        b_ids_to_batch(m_bids, batch["mkpts0_f"])[0][index].detach().clone().cpu()
    )
    points_f1 = (
        b_ids_to_batch(m_bids, batch["mkpts1_f"])[0][index].detach().clone().cpu()
    )
    mconf = (
        b_ids_to_batch(m_bids, batch["mconf"])[0][index].detach().clone().cpu().numpy()
    )

    image0 = tensor_to_image(image_t0)
    image1 = tensor_to_image(image_t1)

    cmap = mpl.colormaps["viridis"]
    colors = [cmap(confidence) for confidence in mconf]

    with plt.ioff():
        fig = make_matching_figure(
            image0, image1, points_f0, points_f1, color=colors, dpi=300
        )
        fig.canvas.draw()
        image = Image.frombytes(
            "RGB", fig.canvas.get_width_height(), fig.canvas.tostring_rgb()
        )

        plt.close(fig)

    return {"matches": pil_to_tensor(image)}


def visualize_predictions(
    batch: dict[str, Any], image0: Tensor, image1: Tensor, index: int = 0
) -> dict[str, Any]:
    image0, image1 = image0.detach().clone(), image1.detach().clone()
    points0 = batch["grid_pt0_i"][index].detach().clone()
    points1 = batch["grid_pt1_i"][index].detach().clone()
    points_w0 = batch["w_pt0_i"][index].detach().clone()
    points_w1 = batch["w_pt1_i"][index].detach().clone()

    b_ids = batch["b_ids"].detach().clone()
    gt_mask = b_ids_to_batch(b_ids, batch["gt_mask"])[0][index].detach().clone()
    i_ids = b_ids_to_batch(b_ids, batch["i_ids"])[0][index][~gt_mask].detach().clone()
    j_ids = b_ids_to_batch(b_ids, batch["j_ids"])[0][index][~gt_mask].detach().clone()
    m_bids = batch["m_bids"].detach().clone()
    points_c0 = b_ids_to_batch(m_bids, batch["mkpts0_c"])[0][index].detach().clone()
    points_c1 = b_ids_to_batch(m_bids, batch["mkpts1_c"])[0][index].detach().clone()
    points_f0 = b_ids_to_batch(m_bids, batch["mkpts0_f"])[0][index].detach().clone()
    points_f1 = b_ids_to_batch(m_bids, batch["mkpts1_f"])[0][index].detach().clone()
    conf_matrix_gt = batch["conf_matrix_gt"][index].detach().clone()

    if "corners0" in batch and "image_orig0" in batch:
        corner0 = batch["corners0"][index, 0]
        corner1 = batch["corners1"][index, 0]

        points0 = points0 + corner0
        points1 = points1 + corner1
        points_w1 = points_w1 + corner0
        points_w0 = points_w0 + corner1
        points_c0 = points_c0 + corner0
        points_c1 = points_c1 + corner1
        points_f0 = points_f0 + corner0
        points_f1 = points_f1 + corner1

    matches_gt = conf_matrix_gt.nonzero()
    matches_correct = conf_matrix_gt[i_ids, j_ids].bool()

    image_pfc0 = draw_points_on_image(
        image0,
        points0,
        points_w1,
        points_c0,
        points_f0,
        matches_correct,
        matches_gt[:, 0],
    )
    image_pfc1 = draw_points_on_image(
        image1,
        points1,
        points_w0,
        points_c1,
        points_f1,
        matches_correct,
        matches_gt[:, 1],
    )

    return {"image_pfc0": image_pfc0, "image_pfc1": image_pfc1}


def draw_points_on_image(
    image: Tensor,
    points: Tensor,
    points_w: Tensor,
    points_c: Tensor,
    points_f: Tensor,
    matches_correct: Tensor,
    points_gt_index: Tensor,
    seed: int = 42,
) -> Tensor:
    points_gt_mask = torch.zeros_like(points[:, 0], dtype=torch.bool)
    points_gt_mask[points_gt_index] = True
    points_gt = points[points_gt_mask]
    points_not_gt = points[~points_gt_mask]

    # Draw grid
    image = draw_points(image, points_not_gt, radius=1, thickness=1, color=(100, 0, 0))
    image = draw_points(image, points_gt, radius=1, thickness=1, color=(0, 100, 0))

    # Draw warped points
    image = draw_points(image, points_w, radius=1, thickness=1, color=(255, 255, 255))

    # Draw coarse false/true points
    if len(matches_correct) == len(points_c):
        points_c_correct = points_c[matches_correct]
        points_c_incorrect = points_c[~matches_correct]
        image = draw_points(
            image, points_c_correct, radius=5, thickness=2, color=(0, 255, 0)
        )
        image = draw_points(
            image, points_c_incorrect, radius=7, thickness=2, color=(255, 0, 0)
        )
    else:
        colormap = mpl.colormaps["hsv"].resampled(points_f.shape[0])
        generator = torch.Generator().manual_seed(seed)
        gradient = colormap(
            torch.linspace(0, 1, points_f.shape[0])[
                torch.randperm(points_f.shape[0], generator=generator)
            ]
        )

        image = draw_points(image, points_c, radius=5, thickness=2, colors=gradient)

    # Draw fine that are different from coarse
    cf_diff = points_c != points_f
    cf_diff = cf_diff[:, 0] & cf_diff[:, 1]
    points_f_diff = points_f[cf_diff]
    image = draw_points(image, points_f_diff, radius=3, thickness=1, color=(0, 0, 0))

    return image


# https://kornia-tutorials.readthedocs.io/en/latest/geometric_transforms.html
def draw_points(
    img_t: "Tensor",
    points: "Tensor",
    radius: int = 3,
    color: tuple[int, int, int] = (255, 255, 255),
    colors: list[tuple[int, int, int]] | None = None,
    thickness: int = 2,
) -> "Tensor":
    """Utility function to draw a set of points in an image."""
    img = K.utils.tensor_to_image(img_t)

    if len(points.shape) == 3:
        points = points[0]

    if len(img.shape) == 4:
        img = img[0]

    img_out = img.copy()

    if colors is None:
        colors = [color] * len(points)

    for pt, color in zip(points, colors, strict=True):
        x, y = int(pt[0]), int(pt[1])
        img_out = cv2.circle(
            img_out, (x, y), radius=radius, color=color, thickness=thickness
        )
    return K.utils.image_to_tensor(img_out).to(img_t.device).clip(0, 1)


def draw_bbox_from_points(
    image: "Tensor", points: "Tensor", color: "Tensor"
) -> "Tensor":
    if len(points.shape) == 2:
        points = points[None]

    if len(image.shape) == 3:
        image = image[None]

    return draw_convex_polygon(image, points, color)[0].float()
