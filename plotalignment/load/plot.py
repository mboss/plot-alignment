from typing import Any

import geopandas as gpd
import pandas as pd
import torch
from kornia.geometry import undistort_points
from torch import Tensor

from plotalignment.transforms.functional import sort_corners


def get_geometry_from_reference(
    sample: dict[str, Any],
    rescale: bool = False,
    image_size_original: Tensor | None = None,
    image_size: Tensor | None = None,
    image_size_original_name: str = "image_size_original",
    image_size_name: str = "image_size0",
    undistort: bool = False,
    camera_matrix: Tensor | None = None,
    distortion_coeffs: Tensor | None = None,
    path_name: str = "path_points",
    polygon_name: str = "polygon",
) -> dict[str, Any]:
    calibration = gpd.read_file(sample[path_name])
    polygon = torch.as_tensor(
        calibration["geometry"][0].exterior.coords, dtype=torch.float64
    )[:4]

    if rescale:
        if image_size_original is None:
            image_size_original = sample[image_size_original_name]
        if image_size is None:
            image_size = sample[image_size_name]

        scale_factor = image_size_original / image_size
        polygon = polygon * scale_factor[[1, 0]]

    if undistort:
        if camera_matrix is None:
            camera_matrix = sample["camera_matrix"]
        if distortion_coeffs is None:
            distortion_coeffs = sample["distortion_coeffs"]

        polygon = undistort_points(
            polygon[None], camera_matrix[None], distortion_coeffs[None]
        )[0]

    polygon = sort_corners(polygon)
    sample[polygon_name] = polygon

    return sample


def extract_plot_corners(
    points_to_extract: list[dict[str, Any]] | pd.DataFrame
) -> Tensor:
    if isinstance(points_to_extract, pd.DataFrame):
        points = [
            (float(s["X"]), float(s["Y"]), float(s["Z"]))
            for _, s in points_to_extract.iterrows()
        ]
    else:
        points = [
            (float(s["X"]), float(s["Y"]), float(s["Z"])) for s in points_to_extract
        ]

    corners = torch.as_tensor(points, dtype=torch.float64)
    sorted_corners = sort_corners(corners).numpy()

    return torch.as_tensor(sorted_corners, dtype=torch.float64)
