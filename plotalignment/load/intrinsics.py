from pathlib import Path
from typing import Any

import cv2
import torch


def get_intrinsic_matrices(calibration_path_str: str) -> dict[str, Any]:
    calibration_path = Path(calibration_path_str).expanduser().resolve()

    fs = cv2.FileStorage(str(calibration_path), cv2.FILE_STORAGE_READ)
    if fs is None:
        raise FileNotFoundError(f"Could not open {calibration_path}.")

    camera_matrix = torch.as_tensor(
        fs.getNode("Camera_Matrix").mat(), dtype=torch.double
    )

    distortion_coeffs = torch.as_tensor(
        fs.getNode("Distortion_Coefficients").mat(), dtype=torch.double
    )[..., 0]

    return {"camera_matrix": camera_matrix, "distortion_coeffs": distortion_coeffs}
