from typing import Any

import numpy as np
import pandas as pd
import torch
from kornia.geometry import undistort_points
from torch import Tensor

from plotalignment.transforms.functional import sort_corners


def process_manual_reference(
    sample: dict[str, Any],
    undistort: bool = False,
    rescale: bool = False,
    camera_matrix: Tensor | None = None,
    distortion_coeffs: Tensor | None = None,
    path_name: str = "path_points",
) -> dict[str, Any]:
    """Process a manual reference sample.
    Reads the csv file and converts the data to tensor points.

    Args:
        sample (dict[str, Any]): sample to process

    Returns:
        dict[str, Any]: processed sample
    """
    df_points = pd.read_csv(sample[path_name])

    points0 = torch.from_numpy(
        df_points[["X_src", "Y_src"]].to_numpy(dtype=np.float64).reshape(-1, 2)
    )
    points1 = torch.from_numpy(
        df_points[["X_dst", "Y_dst"]].to_numpy(dtype=np.float64).reshape(-1, 2)
    )

    if rescale:
        scale_factor = sample["image_size_original"] / sample["image_size0"]
        points0 = points0 * scale_factor[[1, 0]]
        points1 = points1 * scale_factor[[1, 0]]

    if undistort:
        if camera_matrix is None:
            camera_matrix = sample["camera_matrix"]
        if distortion_coeffs is None:
            distortion_coeffs = sample["distortion_coeffs"]

        points0 = undistort_points(
            points0[None], camera_matrix[None], distortion_coeffs[None]
        )[0]

        points1 = undistort_points(
            points1[None], camera_matrix[None], distortion_coeffs[None]
        )[0]

    sample["points0"] = sort_corners(points0)
    sample["points1"] = sort_corners(points1)

    return sample
