import abc
from collections.abc import Callable
from functools import partial
from operator import itemgetter
from pathlib import Path
from typing import Any, cast

import dill as pickle
import torch.multiprocessing as mp
from fastcore.basics import patch
from fastcore.utils import is_listy
from omegaconf import DictConfig
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader
from torchdata.dataloader2 import (
    DataLoader2,
    MultiProcessingReadingService,
    communication,
)
from torchdata.dataloader2.graph import DataPipe
from torchdata.dataloader2.reading_service import (
    WorkerInfo,
    _DummyIterDataPipe,
    _find_replicable_branches,
    attach_wrapper,
    find_lca_round_robin_sharding_dp,
    process_init_fn,
    replace_dp,
    traverse_dps,
)
from torchdata.datapipes.iter import IterDataPipe

from plotalignment.utils import to_pickle, to_tuple

pickle.settings["byref"] = True
pickle.extend(use_dill=False)


class DataPipeDataModule(LightningDataModule, abc.ABC):
    def __init__(
        self,
        num_workers: int | None = None,
        prefetch_factor: int | None = None,
        pin_memory: bool | None = None,
        seed: int | None = None,
        train_batch_size: int | None = None,
        val_batch_size: int | None = None,
        test_batch_size: int | None = None,
        predict_batch_size: int | None = None,
        use_cache: bool | None = None,
        cache_dir: str | None = None,
        cache_name: str | None = None,
        cache_key: str | None = None,
        cache_timeout: int | None = None,
        cfg: dict[str, Any] | DictConfig | None = None,
        **kwargs: Any,
    ) -> None:
        super().__init__()

        args = {
            "num_workers": num_workers,
            "prefetch_factor": prefetch_factor,
            "pin_memory": pin_memory,
            "seed": seed,
            "train_batch_size": train_batch_size,
            "val_batch_size": val_batch_size,
            "test_batch_size": test_batch_size,
            "predict_batch_size": predict_batch_size,
            "use_cache": use_cache,
            "cache_dir": cache_dir,
            "cache_name": cache_name,
            "cache_key": cache_key,
            "cache_timeout": cache_timeout,
        }
        args_exist = {k: v for k, v in args.items() if v is not None}
        kwargs_exist = {k: v for k, v in kwargs.items() if v is not None}

        if cfg is None:
            cfg = {}

        self.cfg = args | dict(cfg) | args_exist | kwargs_exist

    def setup(self, stage: str) -> None:
        """Setup the data module for a given stage.

        Args:
            stage (str): stage to setup
        """
        self.prepare_functions()

        if self.cfg["use_cache"]:
            cache_dir_path = Path(cast(str, self.cfg["cache_dir"]))
            cache_name = cast(str, self.cfg["cache_name"])
            for stage_real in ["train", "val", "test", "predict"]:
                (cache_dir_path / cache_name / stage_real).mkdir(
                    parents=True, exist_ok=True
                )

        match stage:
            case "fit":
                self.setup_dp_train()
                self.setup_dp_val()
            case "validate":
                self.setup_dp_val()
            case "predict":
                self.setup_dp_predict()
            case "test":
                self.setup_dp_test()
            case _:
                raise ValueError(f"Stage {stage} not supported.")

    def prepare_functions(self) -> None:
        pass

    def setup_dp_train(self) -> IterDataPipe:
        self.train_dp = self.process_dp(stage="train")

    def setup_dp_val(self) -> IterDataPipe:
        self.val_dp = self.process_dp(stage="val")

    def setup_dp_test(self) -> IterDataPipe:
        self.test_dp = self.process_dp(stage="test")

    def setup_dp_predict(self) -> IterDataPipe:
        self.predict_dp = self.process_dp(stage="predict")

    @abc.abstractmethod
    def process_dp(self, stage: str) -> IterDataPipe:
        raise NotImplementedError

    def _prepare_caching(
        self, dp: IterDataPipe, stage: str | None = None
    ) -> IterDataPipe:
        if any(
            attr is None
            for attr in [
                self.cfg["cache_dir"],
                self.cfg["cache_name"],
                self.cfg["cache_key"],
            ]
        ):
            raise ValueError(
                "cache_dir, cache_name and cache_key must be specified to enable caching."
                f"Currently set to {self.cfg['cache_dir']}, {self.cfg['cache_name']}, {self.cfg['cache_key']}."
            )

        return dp.on_disk_cache_ordered(
            filepath_fn=partial(
                cache_files,
                cache_dir=self.cfg["cache_dir"],
                cache_name=self.cfg["cache_name"],
                cache_key=self.cfg["cache_key"],
                stage=stage,
            )
        )

    def _end_caching(
        self, dp: "IterDataPipe", stage: str | None = None
    ) -> IterDataPipe:
        return (
            dp.map(to_tuple)
            .map(to_pickle, input_col=1)
            .end_caching_ordered(mode="wb", same_filepath_fn=True)
            .open_files(mode="b")
            .map(itemgetter(1))
            .map(pickle.load)
        )

    def train_dataloader(self) -> DataLoader2:
        self.train_dl = self.setup_dataloader(stage="train")
        return self.train_dl

    def val_dataloader(self) -> DataLoader2:
        self.val_dl = self.setup_dataloader(stage="val")
        return self.val_dl

    def test_dataloader(self) -> DataLoader2:
        self.test_dl = self.setup_dataloader(stage="test")
        return self.test_dl

    def predict_dataloader(self) -> DataLoader2:
        self.predict_dl = self.setup_dataloader(stage="predict")
        return self.predict_dl

    def setup_dataloader(
        self, stage: str, collate_fn: Callable[[Any], Any] | None = None
    ) -> DataLoader2:
        dp = getattr(self, f"{stage}_dp")

        if is_listy(dp):
            return [
                self.initalize_dataloader(datapipe, stage, collate_fn)
                for datapipe in dp
            ]

        return self.initalize_dataloader(dp, stage, collate_fn)

    def initalize_dataloader(
        self,
        dp: IterDataPipe,
        stage: str | None = None,
        collate_fn: Callable[[Any], Any] | None = None,
        batch_size: int | None = None,
    ) -> DataLoader2:
        if batch_size is None:
            batch_size = self.cfg[f"{stage}_batch_size"] if stage is not None else None

        prefetch_factor = self.cfg["prefetch_factor"]
        persistent_workers = True
        if self.cfg["num_workers"] == 0:
            prefetch_factor = None
            persistent_workers = False

        return DataLoader(
            dataset=dp,
            batch_size=batch_size,
            shuffle=stage == "train",
            num_workers=self.cfg["num_workers"],
            collate_fn=collate_fn,
            pin_memory=self.cfg["pin_memory"],
            drop_last=stage in ["train", "val"],
            prefetch_factor=prefetch_factor,
            persistent_workers=persistent_workers,
        )


def cache_files(
    sample: dict[str, Any],
    cache_dir: str,
    cache_name: str,
    cache_key: str,
    stage: str | None = None,
) -> str:
    if stage is None:
        stage = ""

    data_dir = Path(cache_dir).expanduser().resolve()
    filename = Path(sample[cache_key]).stem

    return str(data_dir / cache_name / stage / filename)


@patch
def initialize(self: MultiProcessingReadingService, datapipe: DataPipe):
    if not self._mp:
        # TODO(616): Warn and recommend usage of InProcessReadingService
        worker_info = WorkerInfo(1, 0)
        datapipe = process_init_fn(datapipe, worker_info, self.worker_init_fn)
        self._end_datapipe = datapipe
        return datapipe

    ctx = mp.get_context(self.multiprocessing_context)

    # Launch dispatching process for the lowest common ancestor of non-replicable DataPipes
    graph = traverse_dps(datapipe)
    dispatching_dp = find_lca_round_robin_sharding_dp(graph)
    # TODO(ejguan): When the last DataPipe is round_robin_sharding, use InPrcoessReadingService
    if dispatching_dp is not None:
        dummy_dp = _DummyIterDataPipe()
        graph = replace_dp(graph, dispatching_dp, dummy_dp)
        datapipe = next(iter(graph.values()))[0]
        # TODO(ejguan): Determine buffer_size at runtime or use unlimited buffer
        round_robin_dps = dispatching_dp.round_robin_demux(
            num_instances=self.num_workers, buffer_size=-1
        )
        # TODO(ejguan): Benchmark if we need to prefetch in dispatching process
        worker_info = WorkerInfo(self.num_workers, 0)
        (
            process,
            req_queues,
            res_queues,
        ) = communication.eventloop.CreateProcessForMultipleDataPipelines(
            ctx,
            round_robin_dps,
            process_name="dispatching process",
            worker_info=worker_info,
            custom_reset_fn=self.worker_reset_fn,
        )
        assert len(req_queues) == self.num_workers
        assert len(res_queues) == self.num_workers
        for req_queue in req_queues:
            req_queue.cancel_join_thread()
        for res_queue in res_queues:
            res_queue.cancel_join_thread()
        process.daemon = True
        process.start()
        self._dispatch_process = (process, req_queues, res_queues)

    # Find replicable branches for worker processes
    # The rest of non-replicable DataPipes will remain in the main process
    replicable_dps = _find_replicable_branches(graph)
    assert (
        len(replicable_dps) == 1
    ), "MultiProcessingReadingService only supports single replicable branch currently"
    replicable_dp = replicable_dps[0]
    replicable_dp = attach_wrapper(replicable_dp)

    for worker_id in range(self.num_workers):
        worker_info = WorkerInfo(self.num_workers, worker_id)
        # Dispatching process for non-replicable DataPipes exists
        dispatching_req_queue = (
            None
            if self._dispatch_process is None
            else self._dispatch_process[1][worker_id]
        )
        dispatching_res_queue = (
            None
            if self._dispatch_process is None
            else self._dispatch_process[2][worker_id]
        )
        call_on_process_init = partial(
            process_init_fn,
            worker_info=worker_info,
            custom_init_fn=self.worker_init_fn,
            worker_prefetch_cnt=self.worker_prefetch_cnt,
            dispatching_req_queue=dispatching_req_queue,
            dispatching_res_queue=dispatching_res_queue,
        )
        (
            process,
            req_queue,
            res_queue,
        ) = communication.eventloop.CreateProcessForDataPipeline(
            ctx,
            replicable_dp,
            process_name="worker process",
            worker_info=worker_info,
            call_on_process_init=call_on_process_init,
            custom_reset_fn=self.worker_reset_fn,
        )
        req_queue.cancel_join_thread()
        process.daemon = True
        process.start()
        self._worker_processes.append(
            (process, req_queue, res_queue)
        )  # These queues are independent
        local_datapipe = communication.iter.QueueWrapper(
            communication.protocol.IterDataPipeQueueProtocolClient(req_queue, res_queue)
        )
        self._worker_datapipes.append(local_datapipe)

    end_datapipe = communication.iter._IterateQueueDataPipes(  # noqa
        self._worker_datapipes
    )
    self._worker_consumer_datapipe = end_datapipe

    if self.main_prefetch_cnt > 0:
        end_datapipe = self._worker_consumer_datapipe.prefetch(self.main_prefetch_cnt)
        self._main_prefetch_datapipe = end_datapipe

    # Attach non-replicable DataPipes
    if replicable_dps[0] is not datapipe:
        graph = replace_dp(graph, replicable_dps[0], end_datapipe)
        end_datapipe = datapipe  # type: ignore[assignment]

    self._end_datapipe = end_datapipe
    assert self._end_datapipe is not None

    return self._end_datapipe  # type: ignore[return-value]
