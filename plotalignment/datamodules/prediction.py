import shutil
from collections.abc import Callable, Iterable
from datetime import date, datetime, time
from functools import partial
from itertools import islice
from operator import itemgetter
from pathlib import Path
from typing import Any, cast

import dill as pickle
import geopandas as gpd
import pandas as pd
import rawpy
import torch
from kornia.geometry import undistort_image
from loguru import logger
from omegaconf import DictConfig
from shapely import Polygon
from torch import Tensor
from torch.nn.functional import interpolate
from torch.utils.data import DataLoader
from torchdata.datapipes.iter import FileLister, IterableWrapper, IterDataPipe
from torchdata.datapipes.iter.util.sharding import SHARDING_PRIORITIES
from torchvision.io import ImageReadMode, read_image

from plotalignment.load import (
    extract_plot_corners,
    get_geometry_from_reference,
    get_intrinsic_matrices,
    process_manual_reference,
)
from plotalignment.transforms.functional import (
    calculate_homographies,
    check_initial_reference_validity,
    check_reference_validity,
    postprocess_raw,
    to_tensor_f32,
    xyz_to_rgb_01,
)
from plotalignment.utils import (
    apply_transforms,
    combine_dicts,
    consume,
    filter_by_df,
    read_filenames_regex_pd,
    to_batch,
)

from .datapipe import DataPipeDataModule

pickle.extend(use_dill=False)


class AlignmentPredictionDataModule(DataPipeDataModule):
    def __init__(
        self,
        datapipe: dict[str, Any] | DictConfig,
        year: int | None = None,
        experiment_number: str | None = None,
        save_path_loadable_images: str | None = None,
        save_path_preprocessed_images: str | None = None,
        save_path_initial: str | None = None,
        save_path_aligned: str | None = None,
        save_path_correct_initial_refs: str | None = None,
        save_path_homography: str | None = None,
        save_path_coordinates: str | None = None,
        save_path_unalignable: str | None = None,
        save_path_matches: str | None = None,
        use_homography: bool | None = None,
        use_matches: bool | None = None,
        cfg: dict[str, Any] | DictConfig | None = None,
        **kwargs: Any,
    ) -> None:
        self.cfg: dict[str, Any]
        kwargs = {
            "year": year,
            "experiment_number": experiment_number,
            "save_path_loadable_images": save_path_loadable_images,
            "save_path_preprocessed_images": save_path_preprocessed_images,
            "save_path_initial": save_path_initial,
            "save_path_aligned": save_path_aligned,
            "save_path_correct_initial_refs": save_path_correct_initial_refs,
            "save_path_homography": save_path_homography,
            "save_path_coordinates": save_path_coordinates,
            "save_path_unalignable": save_path_unalignable,
            "save_path_matches": save_path_matches,
            "use_homography": use_homography,
            "use_matches": use_matches,
        } | kwargs

        super().__init__(cfg=cfg, **kwargs)

        self.cfg_datapipes = datapipe

    def prepare_data(self) -> None:
        self.setup_transforms()

        self.prepare_preprocessed_images_file()
        self.prepare_correct_initial_references()
        self.prepare_aligned()
        self.prepare_manual_references()
        self.prepare_coordinates()

        self.prepare_unalignable()
        self.prepare_matches()

    def prepare_preprocessed_images_file(self) -> None:
        if not Path(self.cfg["save_path_preprocessed_images"]).exists():
            logger.info("Preparing images file...")

            df_images = read_filenames_regex_pd(
                **self.cfg_datapipes["remote"]["png"]["pipe"],
                path_column_name="path_image",
            )

            df_images.to_parquet(self.cfg["save_path_preprocessed_images"], index=False)

            logger.success("Images file prepared.")

    def prepare_correct_initial_references(self) -> None:
        if not Path(self.cfg["save_path_initial"]).exists():
            logger.info(
                "Correct initial references file does not exist. Saving correct initial references file at: "
                f"{self.cfg['save_path_correct_initial_refs']}."
            )

            cfg_references = self.cfg_datapipes["remote"]["initial"]["pipe"].copy()
            df_references = read_filenames_regex_pd(
                **cfg_references, path_column_name="path_points"
            )

            df_loadable_images = pd.read_parquet(self.cfg["save_path_loadable_images"])

            df_reference_images = df_references.merge(
                df_loadable_images,
                on=["yearly_experiment_number", "plot_uid", "date", "time"],
                how="inner",
                suffixes=("", "_image"),
                validate="one_to_one",
            )

            def load_initial_reference_with_image(
                sample: dict[str, Any]
            ) -> dict[str, Any]:
                image = cast(
                    Tensor,
                    apply_transforms(
                        sample["path_image"],
                        transforms=self.transforms["predict"]["image_load_raw"],
                    ),
                )
                image_size_original = torch.as_tensor(self.cfg["image_size_original"])
                image_size = torch.as_tensor(image.shape[-2:])

                sample = get_geometry_from_reference(
                    sample,
                    rescale=True,
                    image_size_original=image_size_original,
                    image_size=image_size,
                    undistort=True,
                    **self.intrinsics,
                )

                return sample

            def save_gdf(sample: dict[str, Any]) -> bool:
                image_name = Path(sample["path_points"]).stem
                gdf = gpd.GeoDataFrame(
                    {"image": [image_name]},
                    geometry=[Polygon(sample["polygon"].numpy())],
                )
                save_gdf_with_num_steps_pd(gdf, save_path=self.cfg["save_path_initial"])

                return True

            dp_reference_images = (
                IterableWrapper(df_reference_images.to_dict(orient="records"))
                .sharding_filter()
                .sharding_round_robin_dispatch(SHARDING_PRIORITIES.MULTIPROCESSING)
                .map(load_initial_reference_with_image)
                .filter(check_initial_reference_validity)
                .map(save_gdf)
            )

            Path(self.cfg["save_path_initial"]).mkdir(parents=True, exist_ok=True)
            consume(
                DataLoader(
                    dataset=dp_reference_images,
                    batch_size=16,
                    num_workers=self.cfg["num_workers"],
                ),
                progress=True,
            )

            logger.success("Correct initial references file saved.")

    def prepare_aligned(self) -> None:
        if not Path(self.cfg["save_path_aligned"]).exists():
            logger.info(
                "Aligned images do not exist. Copying initial references to: "
                f"{self.cfg['save_path_aligned']}."
            )

            shutil.copytree(
                self.cfg["save_path_initial"], self.cfg["save_path_aligned"]
            )

            logger.success("Aligned images copied.")

    def prepare_manual_references(self) -> None:
        if not Path(self.cfg["save_path_homography"]).exists():
            logger.info(
                "Homographies do not exist. Saving homographies at: "
                f"{self.cfg['save_path_homography']}."
            )

            df_reference = (
                read_filenames_regex_pd(
                    **self.cfg_datapipes["remote"]["reference"]["pipe"],
                    path_column_name="path_homography",
                )
                .dropna()
                .drop_duplicates(
                    ("yearly_experiment_number", "plot_uid", "date0", "date1")
                )
            )
            if df_reference.empty:
                logger.warning("No manual references found.")
                return

            df_reference = df_reference[df_reference["date0"] != df_reference["date1"]]

            df_reference = df_reference.apply(
                lambda x: process_manual_reference(
                    x, path_name="path_homography", **self.intrinsics
                ),
                axis=1,
            )

            df_reference = df_reference[
                df_reference.apply(
                    lambda x: check_reference_validity(
                        x,
                        image_size_original=self.cfg["image_size_original"],
                        camera_matrix=self.intrinsics["camera_matrix"],
                    ),
                    axis=1,
                )
            ]

            df_reference = df_reference.apply(
                lambda x: calculate_homographies(
                    x, camera_matrix=self.intrinsics["camera_matrix"]
                ),
                axis=1,
            )

            df_reference = df_reference.drop(columns=["points0", "points1"])
            df_reference["hom_0to1"] = df_reference["hom_0to1"].apply(
                lambda x: x.flatten().numpy()
            )
            df_reference["hom_1to0"] = df_reference["hom_1to0"].apply(
                lambda x: x.flatten().numpy()
            )

            df_reference.to_parquet(self.cfg["save_path_homography"], index=False)

            logger.success("Homographies saved.")

    def prepare_coordinates(self) -> None:
        if not Path(self.cfg["save_path_coordinates"]).exists():
            logger.info(
                "Coordinates do not exist. Saving coordinates at: "
                f"{self.cfg['save_path_coordinates']}."
            )

            df_points = (
                pd.concat(
                    [
                        pd.read_csv(filename, index_col=0)
                        for filename in FileLister(
                            **self.cfg_datapipes["remote"]["coordinates"]["pipe"]
                        )
                    ]
                )
                .groupby("plot_UID")
                .apply(lambda x: extract_plot_corners(x))
            )
            df_coords = df_points.index.str.extract(
                r"FP(?P<yearly_experiment_number>[A-Z]+\d\d\d)(?P<plot_uid>\d+)",
                expand=True,
            )
            df_coords["plot_corners"] = df_points.apply(
                lambda x: x.numpy().flatten()
            ).values

            df_coords.to_parquet(self.cfg["save_path_coordinates"], index=False)

            logger.success("Coordinates saved.")

    def prepare_unalignable(self) -> None:
        if not Path(self.cfg["save_path_unalignable"]).is_dir():
            Path(self.cfg["save_path_unalignable"]).mkdir(parents=True)
            logger.info(
                f"Save root does not exist. Created {self.cfg['save_path_unalignable']}."
            )

    def prepare_matches(self) -> None:
        if not Path(self.cfg["save_path_matches"]).is_dir():
            Path(self.cfg["save_path_matches"]).mkdir(parents=True)
            logger.info(
                f"Save root does not exist. Created {self.cfg['save_path_matches']}."
            )

    def process_dp(self, stage: str) -> IterDataPipe:
        df_image = pd.read_parquet(self.cfg["save_path_images"])
        df_image["date"] = pd.to_datetime(df_image["date"], format="%Y%m%d").dt.date
        df_image["time"] = pd.to_datetime(df_image["time"], format="%H%M%S").dt.time

        df_homography = pd.DataFrame()
        if self.cfg.get("use_homography", True):
            if Path(self.cfg["save_path_homography"]).exists():
                logger.info("Using homography.")
                df_homography = pd.read_parquet(self.cfg["save_path_homography"])
            else:
                logger.info("Homography file does not exist.")
        else:
            logger.info("Not using homography.")

        df_coords = pd.read_parquet(self.cfg["save_path_coordinates"])
        df_coords["plot_corners"] = df_coords["plot_corners"].apply(
            lambda x: torch.as_tensor(x.copy().reshape(4, 3))
        )

        df_aligned = read_filenames_regex_pd(
            **self.cfg_datapipes["local"]["aligned"]["pipe"],
            path_column_name="path_points",
        )
        df_aligned["date"] = pd.to_datetime(df_aligned["date"], format="%Y%m%d").dt.date
        df_aligned["time"] = pd.to_datetime(df_aligned["time"], format="%H%M%S").dt.time
        df_aligned["num_steps"] = df_aligned["num_steps"].astype(int)

        df_unalignable = read_filenames_regex_pd(
            **self.cfg_datapipes["local"]["unalignable"]["pipe"],
            path_column_name="path_unalignable",
        )

        df_matches = pd.DataFrame()
        if self.cfg.get("use_matches", True):
            logger.info("Using matches.")
            df_matches = read_filenames_regex_pd(
                **self.cfg_datapipes["local"]["matches"]["pipe"],
                path_column_name="path_matches",
            )
        else:
            logger.info("Not using matches.")

        image_size_t = torch.as_tensor(self.cfg["image_size"])

        def date_time_columns_to_str_pandas(df: pd.DataFrame) -> pd.DataFrame:
            if not df.empty:
                for col in [
                    col
                    for col, value in df.iloc[0].items()
                    if isinstance(value, date | time)
                ]:
                    df[col] = df[col].astype(str)

            return df

        def get_pairs_filtered_by_df(
            df_image: pd.DataFrame,
            df_ref: pd.DataFrame,
            keys: tuple[str, ...] = (
                "yearly_experiment_number",
                "plot_uid",
                "date",
                "time",
            ),
            keys_pairs: tuple[str, ...] = ("yearly_experiment_number", "plot_uid"),
        ) -> pd.DataFrame:
            df_exist = df_image.merge(
                df_ref,
                how="inner",
                on=list(keys),
                suffixes=("", "_points"),
                validate="one_to_one",
            )
            df_not_exist = filter_by_df(df_image, df_exist, on=list(keys))

            return df_exist.merge(
                df_not_exist,
                how="inner",
                on=keys_pairs,
                suffixes=("0", "1"),
                validate="many_to_many",
            )

        df_pairs = get_pairs_filtered_by_df(df_image, df_aligned)

        if df_pairs.empty:
            logger.warning("No pairs to process!")
            return IterableWrapper([])

        df_pairs_alignable = df_pairs
        if not df_unalignable.empty:
            df_unalignable["date0"] = pd.to_datetime(
                df_unalignable["date0"], format="%Y%m%d"
            ).dt.date
            df_unalignable["time0"] = pd.to_datetime(
                df_unalignable["time0"], format="%H%M%S"
            ).dt.time
            df_unalignable["date1"] = pd.to_datetime(
                df_unalignable["date1"], format="%Y%m%d"
            ).dt.date
            df_unalignable["time1"] = pd.to_datetime(
                df_unalignable["time1"], format="%H%M%S"
            ).dt.time

            df_pairs_alignable = filter_by_df(
                df_pairs,
                df_unalignable,
                on=[
                    "yearly_experiment_number",
                    "plot_uid",
                    "date0",
                    "time0",
                    "date1",
                    "time1",
                ],
            )

        if not df_matches.empty:
            logger.info("Datapipe contains matches!")

            df_matches["date0"] = pd.to_datetime(
                df_matches["date0"], format="%Y%m%d"
            ).dt.date
            df_matches["time0"] = pd.to_datetime(
                df_matches["time0"], format="%H%M%S"
            ).dt.time
            df_matches["date1"] = pd.to_datetime(
                df_matches["date1"], format="%Y%m%d"
            ).dt.date
            df_matches["time1"] = pd.to_datetime(
                df_matches["time1"], format="%H%M%S"
            ).dt.time

            df_matches_inv = df_matches.copy()
            df_matches_inv = df_matches_inv.rename(
                columns={
                    "date0": "date1",
                    "time0": "time1",
                    "date1": "date0",
                    "time1": "time0",
                }
            )
            df_matches["reversed"] = False
            df_matches_inv["reversed"] = True

            df_matches = pd.concat([df_matches, df_matches_inv])

            df_has_matches = df_pairs_alignable.merge(
                df_matches,
                how="inner",
                on=[
                    "yearly_experiment_number",
                    "plot_uid",
                    "date0",
                    "time0",
                    "date1",
                    "time1",
                ],
                suffixes=("_pairs", "_matches"),
                validate="one_to_one",
            )

            if not df_has_matches.empty:
                df_has_matches["date_difference"] = (
                    (df_has_matches["date1"] - df_has_matches["date0"])
                    .abs()
                    .apply(lambda timedelta: timedelta.days)
                )

                df_has_matches = (
                    df_has_matches.groupby(
                        ["yearly_experiment_number", "plot_uid", "date1"]
                    )
                    .apply(lambda x: x.nsmallest(1, ["num_steps", "date_difference"]))
                    .reset_index(drop=True)
                )

                df_has_matches = df_has_matches.merge(
                    df_coords,
                    how="inner",
                    on=["yearly_experiment_number", "plot_uid"],
                    suffixes=("_coords", "_coords"),
                    validate="many_to_one",
                )
                df_has_matches = date_time_columns_to_str_pandas(df_has_matches)

                return (
                    IterableWrapper(df_has_matches.to_dict(orient="records"))
                    .sharding_filter()
                    .sharding_round_robin_dispatch(SHARDING_PRIORITIES.MULTIPROCESSING)
                    .map(partial(read_matches, size=image_size_t.tolist()))
                    .map(partial(combine_dicts, dict1=self.intrinsics))
                    .map(get_geometry_from_reference)
                )

        if not df_pairs_alignable.empty and self.cfg.get("new_predictions", True):
            logger.info("Datapipe contains image pairs!")

            df_pairs_alignable.loc[:, "date_difference"] = (
                (
                    df_pairs_alignable.loc[:, "date1"]
                    - df_pairs_alignable.loc[:, "date0"]
                )
                .abs()
                .apply(lambda timedelta: timedelta.days)
            )

            df_pairs_alignable = (
                df_pairs_alignable.groupby(
                    ["yearly_experiment_number", "plot_uid", "date1"]
                )
                .apply(lambda x: x.nsmallest(1, ["num_steps", "date_difference"]))
                .reset_index(drop=True)
            )

            df_pairs_alignable = df_pairs_alignable.merge(
                df_coords,
                how="inner",
                on=["yearly_experiment_number", "plot_uid"],
                suffixes=("", "_coords"),
                validate="many_to_one",
            )

            df_pairs_alignable = date_time_columns_to_str_pandas(df_pairs_alignable)

            # Process dp
            return (
                IterableWrapper(df_pairs_alignable.to_dict(orient="records"))
                .sharding_filter()
                .sharding_round_robin_dispatch(SHARDING_PRIORITIES.MULTIPROCESSING)
                .map(
                    partial(
                        combine_dicts,
                        dict1={
                            "image_size_original": torch.as_tensor(
                                self.cfg["image_size_original"]
                            ),
                            "image_size": torch.as_tensor(image_size_t),
                            "scale0": torch.as_tensor(self.transforms[stage]["scale"]),
                            "scale1": torch.as_tensor(self.transforms[stage]["scale"]),
                        }
                        | self.intrinsics,
                    )
                )
                .map(
                    partial(
                        read_images,
                        image_load=self.transforms[stage]["image_load"],
                        image_transforms=self.transforms[stage]["image_transforms"],
                    )
                )
                .map(get_geometry_from_reference)
            )

        if not df_homography.empty:
            df_homography["hom_0to1"] = df_homography["hom_0to1"].apply(
                lambda x: torch.as_tensor(x.copy().reshape(3, 3))
            )
            df_homography["hom_1to0"] = df_homography["hom_1to0"].apply(
                lambda x: torch.as_tensor(x.copy().reshape(3, 3))
            )
            df_homography["date0"] = pd.to_datetime(
                df_homography["date0"], format="%Y%m%d"
            ).dt.date
            df_homography["date1"] = pd.to_datetime(
                df_homography["date1"], format="%Y%m%d"
            ).dt.date

            df_homography_inverse = df_homography.copy()
            df_homography_inverse.rename(
                columns={
                    "date0": "date1",
                    "date1": "date0",
                    "hom_0to1": "hom_1to0",
                    "hom_1to0": "hom_0to1",
                }
            )

            df_homography = pd.concat([df_homography, df_homography_inverse])

            # Multiple images per date => unclear which homography to use => remove
            df_duplicated_images = df_image[
                df_image.duplicated(("yearly_experiment_number", "plot_uid", "date"))
            ]

            df_homography = filter_by_df(
                df_homography,
                df_duplicated_images,
                left_on=["yearly_experiment_number", "plot_uid", "date0"],
                right_on=["yearly_experiment_number", "plot_uid", "date"],
            )

            df_homography = filter_by_df(
                df_homography,
                df_duplicated_images,
                left_on=["yearly_experiment_number", "plot_uid", "date1"],
                right_on=["yearly_experiment_number", "plot_uid", "date"],
            )

            df_has_homography = df_pairs.merge(
                df_homography,
                on=["yearly_experiment_number", "plot_uid", "date0", "date1"],
                how="inner",
                suffixes=("_pairs", "_homography"),
                validate="one_to_many",
            ).drop_duplicates(("yearly_experiment_number", "plot_uid", "date1"))

            if not df_has_homography.empty:
                logger.info("Datapipe contains homographies!")

                df_has_homography = date_time_columns_to_str_pandas(df_has_homography)

                return (
                    IterableWrapper(df_has_homography.to_dict(orient="records"))
                    .sharding_filter()
                    .sharding_round_robin_dispatch(SHARDING_PRIORITIES.MULTIPROCESSING)
                    .map(partial(combine_dicts, dict1=self.intrinsics))
                    .map(get_geometry_from_reference)
                )

        logger.warning("No pairs to process!")
        return IterableWrapper([])

    def setup_transforms(self) -> None:
        """Setup transforms for all stages."""
        self.transforms = {"predict": self.setup_transform("predict")}

    def setup_transform(self, stage: str) -> dict[str, Any]:
        H, W = self.cfg["image_size_original"]
        H_new, W_new = self.cfg["image_size"]
        scale = (W / W_new, H / H_new)
        self.intrinsics = get_intrinsic_matrices(self.cfg["calibration_path"])

        camera_matrix = self.intrinsics["camera_matrix"].clone()
        camera_matrix[0] = camera_matrix[0] / scale[0]
        camera_matrix[1] = camera_matrix[1] / scale[1]

        image_load = [partial(read_image, mode=ImageReadMode.GRAY), to_tensor_f32]
        image_transforms = [
            to_batch,  # interpolate expects a batch
            partial(
                interpolate,
                size=(H_new, W_new),
                mode="bicubic",
                align_corners=True,
                antialias=True,
            ),
            partial(
                undistort_image,
                K=camera_matrix[None].float(),
                dist=self.intrinsics["distortion_coeffs"][None].float(),
            ),
            itemgetter(0),
            partial(torch.clamp, min=0.0, max=1.0),
        ]

        image_load_raw = [rawpy.imread, postprocess_raw, to_tensor_f32, xyz_to_rgb_01]

        return {
            "image_load": image_load,
            "image_load_raw": image_load_raw,
            "image_transforms": image_transforms,
            "scale": scale,
        }


def read_homography(sample: dict[str, Any], size: torch.Size) -> dict[str, Any]:
    sample = sample | {
        "image0": torch.full(size, torch.nan, dtype=torch.double)[None],
        "image1": torch.full(size, torch.nan, dtype=torch.double)[None],
        "image_size0": torch.as_tensor(size),
        "image_size1": torch.as_tensor(size),
        "scale0": torch.as_tensor(1.0),
        "scale1": torch.as_tensor(1.0),
        "mkpts0_f": torch.full((10000, 2), torch.nan, dtype=torch.double),
        "mkpts1_f": torch.full((10000, 2), torch.nan, dtype=torch.double),
    }
    return sample


def read_matches(sample: dict[str, Any], size: torch.Size) -> dict[str, Any]:
    matches = pd.read_pickle(sample["path_matches"])
    is_reversed = sample["reversed"] if "reversed" in sample else False
    sample["mkpts0_f"] = matches["mkpts0_f"] if not is_reversed else matches["mkpts1_f"]
    sample["mkpts1_f"] = matches["mkpts1_f"] if not is_reversed else matches["mkpts0_f"]
    sample["mkpts0_f_inv"] = (
        matches["mkpts0_f_inv"] if not is_reversed else matches["mkpts1_f_inv"]
    )
    sample["mkpts1_f_inv"] = (
        matches["mkpts1_f_inv"] if not is_reversed else matches["mkpts0_f_inv"]
    )
    return sample


def notany_itemgetter(sample: dict[str, Any], keys: list[str]) -> bool:
    return not any(itemgetter(*keys)(sample))


def read_images(
    sample: dict[str, Any],
    image_load: Iterable[Callable[[Any], Any]],
    image_transforms: Iterable[Callable[[Any], Any]],
) -> dict[str, Any]:
    image_path0 = sample["path_image0"]
    image_path1 = sample["path_image1"]

    image0 = cast(Tensor, apply_transforms(image_path0, transforms=image_load))
    image1 = cast(Tensor, apply_transforms(image_path1, transforms=image_load))

    sample["image_size0"] = torch.as_tensor(image0.shape[-2:])
    sample["image_size1"] = torch.as_tensor(image1.shape[-2:])

    image0 = apply_transforms(image0, transforms=image_transforms)
    image1 = apply_transforms(image1, transforms=image_transforms)

    sample["image0"] = image0
    sample["image1"] = image1

    return sample


def save_gdf_with_num_steps_pd(
    gdf: gpd.GeoDataFrame, save_path: str, path_name: str = "path", num_steps: int = 0
) -> None:
    name = gdf["image"][0]
    filename = f"{name}_{num_steps}.geojson"
    filepath = Path(save_path) / filename
    gdf.to_file(f"{filepath}.tmp", driver="GeoJSON")
    Path(f"{filepath}.tmp").rename(filepath)


def save_gdf_with_num_steps(
    sample: dict[str, Any], save_path: str, path_name: str = "path", num_steps: int = 0
) -> None:
    gdf = sample["gdf"]
    path = Path(sample[path_name])
    filename = f"{path.stem}_{num_steps}{path.suffix}"
    filepath = Path(save_path) / filename
    gdf.to_file(f"{filepath}.tmp", driver="GeoJSON")
    Path(f"{filepath}.tmp").rename(filepath)


def combine_parts_to_filename(
    sample: dict[str, Any], keys: list[str], initial_string: str = ""
) -> str:
    items = itemgetter(*keys)(sample)
    return initial_string + "".join(map(str, items))


def add_num_steps(gdf: gpd.GeoDataFrame, num_steps: int = 0) -> gpd.GeoDataFrame:
    gdf["num_steps"] = num_steps
    return gdf


def key_in_dict(key: str, d: dict[str, Any]) -> bool:
    return key in d


def key_not_in_dict(key: str, d: dict[str, Any]) -> bool:
    return key not in d


def date_difference(sample: dict[str, Any]) -> datetime.date:
    date0_str, date1_str = itemgetter("date0", "date1")(sample)
    date0 = datetime.fromisoformat(date0_str)
    date1 = datetime.fromisoformat(date1_str)

    return abs(date1 - date0).days


def islice_kwargs(iterable: tuple[Any, ...], stop: int) -> tuple[Any, ...]:
    return type(iterable)(islice(iterable, stop))
