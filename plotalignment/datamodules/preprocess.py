from functools import partial
from operator import itemgetter
from pathlib import Path
from typing import Any

import dask.dataframe as dd
import dill as pickle
import pandas as pd
import rawpy
import torch
from fastcore.basics import compose
from loguru import logger
from omegaconf import DictConfig
from torch.nn.functional import interpolate
from torch.utils.data.datapipes.iter import IterableWrapper
from torchdata.datapipes.iter import IterDataPipe
from torchdata.datapipes.iter.util.sharding import SHARDING_PRIORITIES

from plotalignment.transforms.functional import (
    check_if_readable_raw,
    postprocess_raw,
    to_tensor_f32,
    xyz_to_rgb_01,
)
from plotalignment.utils import (
    apply_transforms,
    filter_by_df,
    read_filenames_regex_pd,
    to_batch,
)

from .datapipe import DataPipeDataModule

pickle.extend(use_dill=False)


class PreprocessDataModule(DataPipeDataModule):
    def __init__(
        self,
        datapipe: dict[str, Any] | DictConfig,
        save_path_loadable_images: str | None = None,
        cfg: dict[str, Any] | DictConfig | None = None,
        **kwargs: Any,
    ) -> None:
        self.cfg: dict[str, Any]
        kwargs = {"save_path_loadable_images": save_path_loadable_images} | kwargs
        super().__init__(cfg=cfg, **kwargs)

        self.cfg_datapipes = datapipe

    def prepare_data(self) -> None:
        self.setup_transforms()
        self.prepare_loadable_images()

    def prepare_loadable_images(self) -> None:
        df_images = read_filenames_regex_pd(
            **self.cfg_datapipes["remote"]["raw"]["pipe"], path_column_name="path_image"
        ).drop_duplicates(["yearly_experiment_number", "plot_uid", "date", "time"])

        if Path(self.cfg["save_path_loadable_images"]).exists():
            df_loadable_images = pd.read_parquet(self.cfg["save_path_loadable_images"])

            df_images = filter_by_df(
                df_images,
                df_loadable_images,
                on=["yearly_experiment_number", "plot_uid", "date", "time"],
            )

        if not df_images.empty:
            check_readable = compose(itemgetter("path_image"), check_if_readable_raw)

            ddf_images = dd.from_pandas(df_images, chunksize=16)  # type: ignore
            ddf_loadable = ddf_images.apply(
                check_readable, meta=("image", bool), axis=1
            ).compute()
            df_loadable_images = df_images[ddf_loadable]

            if Path(self.cfg["save_path_loadable_images"]).exists():
                df_loadable_images_old = pd.read_parquet(
                    self.cfg["save_path_loadable_images"]
                )
                df_loadable_images = pd.concat(
                    [df_loadable_images_old, df_loadable_images]
                ).drop_duplicates(
                    ["yearly_experiment_number", "plot_uid", "date", "time"]
                )

            df_loadable_images.to_parquet(
                self.cfg["save_path_loadable_images"], index=False
            )

        logger.success("Loadable image file exists.")

    def process_dp(self, stage: str) -> IterDataPipe:
        df_raw = pd.read_parquet(self.cfg["save_path_loadable_images"])

        df_preprocessed = read_filenames_regex_pd(
            **self.cfg_datapipes["remote"]["png"]["pipe"]
        )

        df_todo = filter_by_df(
            df_raw,
            df_preprocessed,
            on=["yearly_experiment_number", "plot_uid", "date", "time"],
        )

        logger.info(f"Number of images to preprocess: {len(df_todo)}.")

        dp = (
            IterableWrapper(df_todo.to_dict("records"))
            .sharding_filter()
            .sharding_round_robin_dispatch(SHARDING_PRIORITIES.MULTIPROCESSING)
            .map(
                partial(
                    apply_transforms, transforms=self.transforms[stage]["image_load"]
                ),
                input_col="path_image",
                output_col="image",
            )
            .map(
                partial(
                    apply_transforms,
                    transforms=self.transforms[stage]["image_transforms"],
                ),
                input_col="image",
            )
        )

        return dp

    def setup_transforms(self) -> None:
        """Setup transforms for all stages."""
        self.transforms = {"predict": self.setup_transform("predict")}

    def setup_transform(self, stage: str) -> dict[str, Any]:
        H_new, W_new = self.cfg["image_size"]

        image_load = [rawpy.imread, postprocess_raw, to_tensor_f32, xyz_to_rgb_01]
        image_transforms = [
            to_batch,  # interpolate expects a batch
            partial(
                interpolate,
                size=(H_new, W_new),
                mode="bicubic",
                align_corners=True,
                antialias=True,
            ),
            itemgetter(0),
            partial(torch.clamp, min=0.0, max=1.0),
        ]

        return {"image_load": image_load, "image_transforms": image_transforms}
