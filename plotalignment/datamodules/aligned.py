from collections.abc import Callable, Iterable
from datetime import date, time
from functools import partial
from operator import itemgetter
from typing import Any, cast

import dill as pickle
import pandas as pd
import torch
from fastcore.basics import map_ex
from kornia.geometry import undistort_image
from omegaconf import DictConfig
from torch import Tensor
from torch.nn.functional import interpolate
from torchdata.dataloader2 import DataLoader2
from torchdata.datapipes.iter import IterableWrapper, IterDataPipe
from torchdata.datapipes.iter.util.sharding import SHARDING_PRIORITIES
from torchvision.io import read_image

from plotalignment.load import get_geometry_from_reference, get_intrinsic_matrices
from plotalignment.transforms.functional import to_tensor_f32
from plotalignment.utils import (
    apply_transforms,
    combine_dicts,
    read_filenames_regex_pd,
    to_batch,
)

from .prediction import AlignmentPredictionDataModule

pickle.extend(use_dill=False)


class AlignedDataModule(AlignmentPredictionDataModule):
    def __init__(
        self,
        datapipe: dict[str, Any] | DictConfig,
        save_root: str | None = None,
        save_path_images: str | None = None,
        save_name_preprocessed_images: str | None = None,
        save_path_homography: str | None = None,
        save_path_unalignable: str | None = None,
        save_path_matches: str | None = None,
        sort_longest_first: bool | None = None,
        cfg: dict[str, Any] | DictConfig | None = None,
        **kwargs: Any,
    ) -> None:
        self.cfg: dict[str, Any]

        kwargs = kwargs | {
            "save_root": save_root,
            "save_path_images": save_path_images,
            "save_name_preprocessed_images": save_name_preprocessed_images,
            "save_path_homography": save_path_homography,
            "save_path_unalignable": save_path_unalignable,
            "save_path_matches": save_path_matches,
            "sort_longest_first": sort_longest_first,
        }

        super().__init__(datapipe=datapipe, cfg=cfg, **kwargs)

    def prepare_functions(self) -> None:
        self.setup_transforms()

    def process_dp(self, stage: str) -> IterDataPipe:
        df_image = pd.read_parquet(self.cfg["save_path_preprocessed_images"])
        df_image["date"] = pd.to_datetime(df_image["date"], format="%Y%m%d").dt.date
        df_image["time"] = pd.to_datetime(df_image["time"], format="%H%M%S").dt.time

        df_aligned = read_filenames_regex_pd(
            **self.cfg_datapipes["local"]["aligned"]["pipe"],
            path_column_name="path_points",
        )
        df_aligned["date"] = pd.to_datetime(df_aligned["date"], format="%Y%m%d").dt.date
        df_aligned["time"] = pd.to_datetime(df_aligned["time"], format="%H%M%S").dt.time

        if "plot_uid" in self.cfg:
            df_aligned = df_aligned[df_aligned["plot_uid"] == self.cfg["plot_uid"]]

        df_coords = pd.read_parquet(self.cfg["save_path_coordinates"])
        df_coords["plot_corners"] = df_coords["plot_corners"].apply(
            lambda x: torch.as_tensor(x.reshape(4, 3).copy())
        )

        image_size_t = torch.as_tensor(self.cfg["image_size"])

        df = df_aligned.merge(
            df_image,
            how="inner",
            on=("yearly_experiment_number", "plot_uid", "date", "time"),
            validate="one_to_one",
        )

        df = df.merge(
            df_coords,
            how="inner",
            on=["yearly_experiment_number", "plot_uid"],
            suffixes=("", "_coords"),
            validate="many_to_one",
        )

        for col in [
            col for col, value in df.iloc[0].items() if isinstance(value, date | time)
        ]:
            df[col] = df[col].astype(str)

        df_grouped = df.groupby("plot_uid")
        groups_sorted = [
            group[1].to_dict("records")
            for group in sorted(
                df_grouped,
                key=lambda x: len(x[1]),
                reverse=self.cfg["sort_longest_first"],
            )
        ]

        def read_image(
            sample: dict[str, Any],
            image_load: Iterable[Callable[[Any], Any]],
            image_transforms: Iterable[Callable[[Any], Any]],
        ) -> dict[str, Any]:
            image_path0 = sample["path_image"]

            image = cast(Tensor, apply_transforms(image_path0, transforms=image_load))
            sample["image_size0"] = torch.as_tensor(image.shape[-2:])
            image = apply_transforms(image, transforms=image_transforms)

            sample["image"] = image

            return sample

        # Process dp
        dp = (
            IterableWrapper(groups_sorted)
            .sharding_filter()
            .sharding_round_robin_dispatch(SHARDING_PRIORITIES.MULTIPROCESSING)
            .map(partial(sorted, key=itemgetter("date", "time")))
            .map(
                partial(
                    map_ex,
                    f=partial(
                        combine_dicts,
                        dict1={
                            "image_size_original": torch.as_tensor(
                                self.cfg["image_size_original"]
                            ),
                            "image_size": torch.as_tensor(image_size_t),
                            "scale0": torch.as_tensor(self.transforms[stage]["scale"]),
                            "scale1": torch.as_tensor(self.transforms[stage]["scale"]),
                        }
                        | self.intrinsics,
                    ),
                )
            )
            .map(
                partial(
                    map_ex,
                    f=partial(
                        read_image,
                        image_load=self.transforms[stage]["image_load"],
                        image_transforms=self.transforms[stage]["image_transforms"],
                    ),
                )
            )
            .map(partial(map_ex, f=get_geometry_from_reference))
        )

        return dp

    def setup_dataloader(
        self, stage: str, collate_fn: Callable[[Any], Any] | None = None
    ) -> DataLoader2:
        dp = getattr(self, f"{stage}_dp").collate(collate_fn=collate_fn)

        return self.initalize_dataloader(dp)

    def setup_transforms(self) -> None:
        """Setup transforms for all stages."""
        self.transforms = {"predict": self.setup_transform("predict")}

    def setup_transform(self, stage: str) -> dict[str, Any]:
        H, W = self.cfg["image_size_original"]
        H_new, W_new = self.cfg["image_size"]
        scale = (W / W_new, H / H_new)
        self.intrinsics = get_intrinsic_matrices(self.cfg["calibration_path"])

        camera_matrix = self.intrinsics["camera_matrix"].clone()
        camera_matrix[0] = camera_matrix[0] / scale[0]
        camera_matrix[1] = camera_matrix[1] / scale[1]

        image_load = [partial(read_image), to_tensor_f32]
        image_transforms = [
            to_batch,  # interpolate expects a batch
            partial(
                interpolate,
                size=(H_new, W_new),
                mode="bicubic",
                align_corners=True,
                antialias=True,
            ),
            partial(
                undistort_image,
                K=camera_matrix[None].float(),
                dist=self.intrinsics["distortion_coeffs"][None].float(),
            ),
            itemgetter(0),
            partial(torch.clamp, min=0.0, max=1.0),
        ]

        return {
            "image_load": image_load,
            "image_transforms": image_transforms,
            "scale": scale,
        }
