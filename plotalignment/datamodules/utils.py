from functools import partial
from operator import itemgetter
from pathlib import Path
from typing import Any, cast

import pandas as pd
import torch
from loguru import logger
from omegaconf import DictConfig
from torch.utils.data import default_collate
from torchdata.datapipes.iter import IterDataPipe

from plotalignment.utils import to_tuple


def get_splits(
    dp: "IterDataPipe", cfg_split: "DictConfig | dict[str, Any]"
) -> tuple["IterDataPipe", "IterDataPipe", "IterDataPipe"]:
    if "total_length" in cfg_split:
        len_dp = cfg_split["total_length"]
        logger.info(f"Total length of dataset set based on config to: {len_dp}")
    else:
        len_dp = len(list(dp))
        logger.info(f"Total length of dataset found as: {len_dp}")

    train_length = int(cfg_split["train"] * len_dp)
    test_length = int(cfg_split["test"] * len_dp)
    val_length = len_dp - train_length - test_length

    train, val, test = dp.random_split(
        weights={"train": train_length, "val": val_length, "test": test_length},
        seed=cfg_split["seed"],
        total_length=len_dp,
    )

    return train, val, test


def save_dp_files(
    dp: IterDataPipe,
    save_root: str | Path,
    filename_col: str,
    save_data: bool = True,
    save_abs_filename: bool = False,
) -> "IterDataPipe":
    dp = (
        dp.map(
            partial(get_filepath_stage, save_root=save_root),
            input_col=filename_col,
            output_col="path_stage",
        )
        .map(to_tuple)
        .map(itemgetter("path_stage"), input_col=0)
    )

    if save_data:
        dp = (
            dp.map(itemgetter(filename_col), input_col=1)
            .map(pd.read_csv, input_col=1)
            .map(pd_to_csv, input_col=1)
        )
    elif save_abs_filename:
        dp = dp.map(itemgetter(filename_col), input_col=1)
    else:
        dp = dp.map(to_tuple).map(to_empty_string, input_col=1)

    dp = dp.save_to_disk()
    return dp


def to_empty_string(sample: Any) -> str:
    return ""


def pd_to_csv(pd: pd.DataFrame) -> str:
    return cast(str, pd.to_csv(index=False))[:-1]


def get_filepath_stage(filepath: str | Path, save_root: str | Path) -> str:
    return str(Path(save_root) / Path(filepath).name)


def collate_fn_cat(
    sample_list: list[dict[str, Any]], keys: list[str]
) -> dict[str, Any]:
    batch_cat = {}
    for key in keys:
        if len(sample_list) > 0 and key in sample_list[0]:
            if key == "spv_b_ids":
                for i, sample in enumerate(sample_list):
                    # FIXME: doesn't work for batched patches
                    sample[key] = sample[key] + i
            batch_cat[key] = torch.cat([sample[key] for sample in sample_list])

    sample_list_without_keys = [
        {k: v for k, v in sample.items() if k not in keys} for sample in sample_list
    ]

    batch = cast(dict[str, Any], default_collate(sample_list_without_keys))
    return batch | batch_cat
