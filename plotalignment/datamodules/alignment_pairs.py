from collections.abc import Callable, Iterable
from functools import partial
from itertools import combinations
from operator import itemgetter
from pathlib import Path
from typing import Any, cast

import dill as pickle
import pandas as pd
import torch
from kornia.geometry import undistort_image
from loguru import logger
from omegaconf import DictConfig
from torch import Tensor
from torch.nn import Module
from torch.nn.functional import interpolate
from torch.utils.data import random_split
from torchdata.dataloader2 import DataLoader2
from torchdata.datapipes.iter import IterableWrapper, IterDataPipe
from torchdata.datapipes.iter.util.sharding import SHARDING_PRIORITIES
from torchvision.io import ImageReadMode, read_image

from plotalignment.load import get_geometry_from_reference, get_intrinsic_matrices
from plotalignment.systems.utils import spvs_coarse
from plotalignment.transforms.functional import (
    calculate_homographies,
    check_reference_validity,
    to_tensor_f32,
)
from plotalignment.utils import (
    apply_transforms,
    combine_dicts,
    dict_from_batch,
    dict_to_batch,
    merge_dicts,
    read_filenames_regex_pd,
    remove_dict_entry,
    to_batch,
)

from .datapipe import DataPipeDataModule
from .utils import collate_fn_cat

pickle.extend(use_dill=False)


class AlignmentPairsDataModule(DataPipeDataModule):
    def __init__(
        self,
        datapipe: dict[str, Any] | DictConfig,
        augmentations: dict[str, Module],
        save_path_images: str | Path | None = None,
        save_path_aligned: str | Path | None = None,
        save_path_splits: str | Path | None = None,
        split_ratios: list[float] | None = None,
        cfg: dict[str, Any] | DictConfig | None = None,
        **kwargs: Any,
    ) -> None:
        self.cfg: dict[str, Any]
        kwargs = {
            "save_path_images": save_path_images,
            "save_path_aligned": save_path_aligned,
            "save_path_splits": save_path_splits,
            "split_ratios": split_ratios,
        } | kwargs
        super().__init__(cfg=cfg, **kwargs)

        self.cfg_datapipes = datapipe
        self.augmentations = augmentations

        self.split_paths = {
            stage: Path(self.cfg["save_path_splits"]) / f"{stage}.parquet"
            for stage in ["train", "val", "test"]
        }

    def prepare_data(self) -> None:
        self.setup_transforms()

        self.prepare_images_file()
        self.prepare_alignments()
        self.prepare_splits()

    def prepare_functions(self) -> None:
        self.setup_transforms()

    def prepare_images_file(self) -> None:
        if not Path(self.cfg["save_path_images"]).exists():
            logger.info("Preparing loadable images...")

            df_images = read_filenames_regex_pd(
                **self.cfg_datapipes["remote"]["png"]["pipe"],
                path_column_name="path_image",
            )

            df_images.to_parquet(self.cfg["save_path_images"], index=False)

            logger.success("Loadable images prepared.")

    def prepare_alignments(self) -> None:
        if not Path(self.cfg["save_path_aligned"]).exists():
            logger.info("Preparing aligments...")

            df_aligned = read_filenames_regex_pd(
                **self.cfg_datapipes["remote"]["aligned"]["pipe"],
                path_column_name="path_points",
            )

            df_aligned.to_parquet(self.cfg["save_path_aligned"])

            logger.success("Alignments prepared.")

    def prepare_splits(self) -> None:
        """Prepare the splits for the manual reference data module.

        Raises:
            ValueError: If some split folders exist, but not all.
        """
        split_filepaths = self.split_paths.values()
        split_files_exist = [Path(dir_).exists() for dir_ in split_filepaths]

        if not all(split_files_exist):
            if any(split_files_exist):
                non_existings_dirs = [
                    split_filepath
                    for split_filepath, split_file_exist in zip(
                        split_filepaths, split_files_exist, strict=True
                    )
                    if not split_file_exist
                ]
                raise ValueError(
                    "Some split files exist, but not all. "
                    "Please delete all split files or none."
                    "The following split files do not exist: "
                    f"{non_existings_dirs}"
                )

            logger.info("No split files exist. Creating them now...")

            df_aligned = pd.read_parquet(self.cfg["save_path_aligned"])

            splits_aligned: list[pd.DataFrame] = [
                pd.DataFrame(list(split))  # type: ignore
                for split in random_split(
                    list(df_aligned.to_dict("records")),  # type: ignore
                    self.cfg["split_ratios"],
                )
            ]

            for split, split_filepath in zip(
                splits_aligned, split_filepaths, strict=True
            ):
                Path(split_filepath).parent.mkdir(parents=True, exist_ok=True)
                pd.DataFrame(split).to_parquet(split_filepath)

            logger.success("All split files created.")

    def process_dp(self, stage: str) -> IterDataPipe:
        df_images = pd.read_parquet(self.cfg["save_path_images"])
        df_aligned = pd.read_parquet(self.split_paths[stage]).drop(
            columns=["num_steps"]
        )

        df_aligned_images = df_aligned.merge(
            df_images,
            how="inner",
            on=["yearly_experiment_number", "plot_uid", "date", "time"],
        )

        dp = IterableWrapper(df_aligned_images.to_dict(orient="records"))

        def merge_tuple(group: tuple[dict[str, Any]]) -> dict[str, Any]:
            return merge_dicts(
                *group, keys_to_rename=["date", "time", "path_image", "path_points"]
            )

        dp = (
            dp.groupby(
                itemgetter("yearly_experiment_number", "plot_uid"), buffer_size=-1
            )
            .flatmap(partial(combinations, r=2))
            .map(merge_tuple)
        )

        if stage == "train":
            dp = dp.shuffle(buffer_size=int(1e8))

        dp = dp.sharding_filter().sharding_round_robin_dispatch(
            SHARDING_PRIORITIES.MULTIPROCESSING
        )

        if self.cfg.get("use_cache", False):
            dp = self._prepare_caching(dp, stage)

        dp = (
            dp.map(
                partial(
                    combine_dicts,
                    dict1={
                        "scale0": torch.as_tensor(self.transforms[stage]["scale"]),
                        "scale1": torch.as_tensor(self.transforms[stage]["scale"]),
                        "image_size_original": torch.as_tensor(
                            self.cfg["image_size_original"]
                        ),
                        "image_size": torch.as_tensor(self.cfg["image_size"]),
                        "coarse_scale": self.cfg["coarse_scale"],
                    }
                    | self.intrinsics,
                )
            )
            .map(
                partial(
                    get_geometry_from_reference,
                    path_name="path_points0",
                    polygon_name="points0",
                )
            )
            .map(
                partial(
                    get_geometry_from_reference,
                    path_name="path_points1",
                    polygon_name="points1",
                )
            )
            .filter(check_reference_validity)
            .map(calculate_homographies)
            .map(
                partial(
                    read_images,
                    image_load=self.transforms[stage]["image_load"],
                    image_transforms=self.transforms[stage]["image_transforms"],
                )
            )
        )

        if self.cfg.get("cache", False):
            dp = self._end_caching(dp, stage)

        dp = dp.map(dict_to_batch)

        if self.cfg.get("get_patches", False):
            dp = dp.map(self.augmentations[stage])

        dp = (
            dp.map(partial(spvs_coarse, scale=self.cfg["coarse_scale"]))
            .map(dict_from_batch)
            .map(partial(remove_dict_entry, key="aug"))
        )

        return dp

    def setup_dataloader(
        self, stage: str, collate_fn: Callable[[Any], Any] | None = None
    ) -> DataLoader2:
        if collate_fn is None and (
            self.cfg.get("get_patches", False)
            or cast(int, self.cfg[f"{stage}_batch_size"]) > 1
        ):
            collate_fn = partial(
                collate_fn_cat, keys=["spv_b_ids", "spv_i_ids", "spv_j_ids"]
            )
        return super().setup_dataloader(stage, collate_fn=collate_fn)

    def setup_transforms(self) -> None:
        """Setup transforms for all stages."""
        self.transforms = {
            "train": self.setup_transform("train"),
            "val": self.setup_transform("val"),
            "test": self.setup_transform("test"),
        }

    def setup_transform(self, stage: str) -> dict[str, Any]:
        H, W = self.cfg["image_size_original"]
        H_new, W_new = self.cfg["image_size"]
        scale = (W / W_new, H / H_new)
        self.intrinsics = get_intrinsic_matrices(self.cfg["calibration_path"])

        camera_matrix = self.intrinsics["camera_matrix"].clone()
        camera_matrix[0] = camera_matrix[0] / scale[0]
        camera_matrix[1] = camera_matrix[1] / scale[1]

        image_load = [partial(read_image, mode=ImageReadMode.GRAY), to_tensor_f32]
        image_transforms = [
            to_batch,  # interpolate expects a batch
            partial(
                interpolate,
                size=(H_new, W_new),
                mode="bicubic",
                align_corners=True,
                antialias=True,
            ),
            partial(
                undistort_image,
                K=camera_matrix[None].float(),
                dist=self.intrinsics["distortion_coeffs"][None].float(),
            ),
            itemgetter(0),
            partial(torch.clamp, min=0.0, max=1.0),
        ]

        return {
            "image_load": image_load,
            "image_transforms": image_transforms,
            "scale": scale,
        }


def read_images(
    sample: dict[str, Any],
    image_load: Iterable[Callable[[Any], Any]],
    image_transforms: Iterable[Callable[[Any], Any]],
) -> dict[str, Any]:
    image_path0 = sample["path_image0"]
    image_path1 = sample["path_image1"]

    image0 = cast(Tensor, apply_transforms(image_path0, transforms=image_load))
    image1 = cast(Tensor, apply_transforms(image_path1, transforms=image_load))

    sample["image_size0"] = torch.as_tensor(image0.shape[-2:])
    sample["image_size1"] = torch.as_tensor(image1.shape[-2:])

    image0 = apply_transforms(image0, transforms=image_transforms)
    image1 = apply_transforms(image1, transforms=image_transforms)

    sample["image0"] = image0
    sample["image1"] = image1

    return sample
