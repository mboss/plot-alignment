from collections.abc import Callable, Iterable
from functools import partial
from operator import itemgetter
from pathlib import Path
from typing import Any, cast

import dill as pickle
import pandas as pd
import torch
from kornia.geometry import undistort_image
from omegaconf import DictConfig
from rawpy import rawpy
from torch import Tensor
from torch.nn.functional import interpolate
from torchdata.datapipes.iter import IterableWrapper, IterDataPipe
from torchdata.datapipes.iter.util.sharding import SHARDING_PRIORITIES
from torchvision.io import ImageReadMode, read_image

from plotalignment.load import get_geometry_from_reference, get_intrinsic_matrices
from plotalignment.transforms.functional import (
    postprocess_raw,
    to_tensor_f32,
    xyz_to_rgb_01,
)
from plotalignment.utils import (
    apply_transforms,
    combine_dicts,
    filter_by_df,
    read_filenames_regex_pd,
    to_batch,
)

from .prediction import AlignmentPredictionDataModule

pickle.extend(use_dill=False)


class PotentiallyCorrectDataModule(AlignmentPredictionDataModule):
    def __init__(
        self,
        datapipe: dict[str, Any] | DictConfig,
        save_path_loadable_image: str | None = None,
        save_path_potentially_correct: str | None = None,
        cfg: dict[str, Any] | DictConfig | None = None,
        **kwargs: Any,
    ) -> None:
        self.cfg: dict[str, Any]
        kwargs = {
            "save_path_loadable_image": save_path_loadable_image,
            "save_path_potentially_correct": save_path_potentially_correct,
        } | kwargs
        super().__init__(datapipe=datapipe, cfg=cfg, **kwargs)

    def prepare_data(self) -> None:
        self.setup_transforms()

    def process_dp(self, stage: str) -> IterDataPipe:
        df_images = pd.read_parquet(self.cfg["save_path_preprocessed_images"])

        df_aligned = read_filenames_regex_pd(
            **self.cfg_datapipes["local"]["aligned"]["pipe"],
            path_column_name="path_points",
        )

        df_potentially_correct = read_filenames_regex_pd(
            **self.cfg_datapipes["local"]["potentially_correct"]["pipe"],
            path_column_name="path_homography",
        )
        df_potentially_correct["num_steps"] = df_potentially_correct[
            "num_steps"
        ].astype(int)

        df_potentially_correct = filter_by_df(
            df_potentially_correct,
            df_aligned,
            left_on=["yearly_experiment_number", "plot_uid", "date1"],
            right_on=["yearly_experiment_number", "plot_uid", "date"],
        )

        df_potentially_correct["date_difference"] = (
            (
                pd.to_datetime(df_potentially_correct["date1"], format="%Y%m%d").dt.date
                - pd.to_datetime(
                    df_potentially_correct["date0"], format="%Y%m%d"
                ).dt.date
            )
            .abs()
            .apply(lambda timedelta: timedelta.days)
        )

        df_potentially_correct = (
            df_potentially_correct.groupby(
                ["yearly_experiment_number", "plot_uid", "date1"]
            )
            .apply(lambda x: x.nsmallest(1, ["num_steps", "date_difference"]))
            .reset_index(drop=True)
        )

        df_initial = read_filenames_regex_pd(
            **self.cfg_datapipes["remote"]["initial"]["pipe"],
            path_column_name="path_points_initial",
        )
        df_initial = (
            df_initial.merge(
                df_aligned,
                how="inner",
                on=["yearly_experiment_number", "plot_uid", "date", "time"],
                validate="one_to_one",
            )
            .drop(columns=["path_points_initial"])
            .rename(columns={"path_points": "path_points_initial"})
        )

        image_size_t = torch.as_tensor(self.cfg["image_size"])

        df = df_aligned.merge(
            df_potentially_correct,
            how="inner",
            left_on=["yearly_experiment_number", "plot_uid", "date", "time"],
            right_on=["yearly_experiment_number", "plot_uid", "date0", "time0"],
            validate="one_to_many",
        ).drop(columns=["date", "time"])

        df = df.merge(
            df_images,
            how="inner",
            left_on=["yearly_experiment_number", "plot_uid", "date0", "time0"],
            right_on=["yearly_experiment_number", "plot_uid", "date", "time"],
        ).drop(columns=["date", "time"])

        df = df.merge(
            df_images,
            how="inner",
            left_on=["yearly_experiment_number", "plot_uid", "date1", "time1"],
            right_on=["yearly_experiment_number", "plot_uid", "date", "time"],
            suffixes=("0", "1"),
        ).drop(columns=["date", "time"])

        df_initial_image = df_initial.merge(
            df_images,
            how="inner",
            left_on=["yearly_experiment_number", "plot_uid", "camera", "date", "time"],
            right_on=["yearly_experiment_number", "plot_uid", "camera", "date", "time"],
        ).rename(columns={"path_image": "path_image_initial"})

        df = df.merge(
            df_initial_image,
            how="inner",
            left_on=["yearly_experiment_number", "plot_uid"],
            right_on=["yearly_experiment_number", "plot_uid"],
            suffixes=("", "_initial"),
        )

        def read_image(
            sample: dict[str, Any],
            image_load: Iterable[Callable[[Any], Any]],
            image_transforms: Iterable[Callable[[Any], Any]],
            image_path_name: str = "path_image",
            image_size_name: str = "image_size",
            image_name: str = "image",
        ) -> dict[str, Any]:
            image_path = sample[image_path_name]
            image = cast(Tensor, apply_transforms(image_path, transforms=image_load))
            sample[image_size_name] = torch.as_tensor(image.shape[-2:])
            image = apply_transforms(image, transforms=image_transforms)
            sample[image_name] = image

            return sample

        def read_homography(sample: dict[str, Any]) -> dict[str, Any]:
            with Path(sample["path_homography"]).open("rb") as f:
                homography = pickle.load(f)

            sample["homography"] = homography

            return sample

        # Process dp
        dp = (
            IterableWrapper(df.to_dict("records"))
            .sharding_filter()
            .sharding_round_robin_dispatch(SHARDING_PRIORITIES.MULTIPROCESSING)
            .map(
                partial(
                    combine_dicts,
                    dict1={
                        "image_size_original": torch.as_tensor(
                            self.cfg["image_size_original"]
                        ),
                        "image_size": torch.as_tensor(image_size_t),
                        "scale0": torch.as_tensor(self.transforms[stage]["scale"]),
                        "scale1": torch.as_tensor(self.transforms[stage]["scale"]),
                    }
                    | self.intrinsics,
                )
            )
            .map(
                partial(
                    read_image,
                    image_load=self.transforms[stage]["image_load"],
                    image_transforms=self.transforms[stage]["image_transforms"],
                    image_path_name="path_image0",
                    image_size_name="image_size0",
                    image_name="image0",
                )
            )
            .map(
                partial(
                    read_image,
                    image_load=self.transforms[stage]["image_load"],
                    image_transforms=self.transforms[stage]["image_transforms"],
                    image_path_name="path_image1",
                    image_size_name="image_size1",
                    image_name="image1",
                )
            )
            .map(
                partial(
                    read_image,
                    image_load=self.transforms[stage]["image_load"],
                    image_transforms=self.transforms[stage]["image_transforms"],
                    image_path_name="path_image_initial",
                    image_size_name="image_size_initial",
                    image_name="image_initial",
                )
            )
            .map(get_geometry_from_reference)
            .map(read_homography)
            .map(
                partial(
                    get_geometry_from_reference,
                    image_size_original_name="image_size_initial",
                    image_size_name="image_size",
                    path_name="path_points_initial",
                    polygon_name="polygon_initial",
                )
            )
        )

        return dp

    def setup_transforms(self) -> None:
        """Setup transforms for all stages."""
        self.transforms = {"predict": self.setup_transform("predict")}

    def setup_transform(self, stage: str) -> dict[str, Any]:
        H, W = self.cfg["image_size_original"]
        H_new, W_new = self.cfg["image_size"]
        scale = (W / W_new, H / H_new)
        self.intrinsics = get_intrinsic_matrices(self.cfg["calibration_path"])

        camera_matrix = self.intrinsics["camera_matrix"].clone()
        camera_matrix[0] = camera_matrix[0] / scale[0]
        camera_matrix[1] = camera_matrix[1] / scale[1]

        image_load = [partial(read_image, mode=ImageReadMode.RGB), to_tensor_f32]
        image_load_raw = [rawpy.imread, postprocess_raw, to_tensor_f32, xyz_to_rgb_01]
        image_transforms = [
            to_batch,  # interpolate expects a batch
            partial(
                interpolate,
                size=(H_new, W_new),
                mode="bicubic",
                align_corners=True,
                antialias=True,
            ),
            partial(
                undistort_image,
                K=camera_matrix[None].float(),
                dist=self.intrinsics["distortion_coeffs"][None].float(),
            ),
            itemgetter(0),
            partial(torch.clamp, min=0.0, max=1.0),
        ]

        return {
            "image_load": image_load,
            "image_load_raw": image_load_raw,
            "image_transforms": image_transforms,
            "scale": scale,
        }
