import torch
from shapely import Polygon
from torch import Tensor


def calculate_patch_covisibility(
    corners_patch0_warped: Tensor, corners_patch1: Tensor
) -> Tensor:
    polygons0 = [Polygon(corners.cpu().numpy()) for corners in corners_patch0_warped]
    polygons1 = [Polygon(corners.cpu().numpy()) for corners in corners_patch1]

    covisibility = [
        polygon0.intersection(polygon1).area / polygon0.union(polygon1).area
        for polygon0, polygon1 in zip(polygons0, polygons1, strict=True)
    ]

    return torch.as_tensor(covisibility, device=corners_patch0_warped.device)
