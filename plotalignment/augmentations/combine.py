from copy import deepcopy

from torch import Tensor
from torch.nn import Module


class Augmentations(Module):
    def __init__(
        self,
        augment_homographies: bool,
        augment_images: bool,
        image_aug: Module,
        homography_aug: Module,
    ) -> None:
        super().__init__()

        self.augment_homographies = augment_homographies
        self.augment_images = augment_images

        self.image_aug = image_aug
        self.homography_aug = homography_aug

    def forward(self, batch: dict[str, Tensor]) -> dict[str, Tensor]:
        if self.augment_homographies:
            batch = deepcopy(self.homography_aug)(batch)

        if self.augment_images:
            batch = self.image_aug(batch)

        return batch
