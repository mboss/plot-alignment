from kornia.augmentation import (
    AugmentationSequential,
    RandomBrightness,
    RandomContrast,
    RandomGaussianNoise,
    RandomMotionBlur,
    RandomSharpness,
)
from torch import Tensor
from torch.nn import Module


class ImageAugmentations(Module):
    def __init__(
        self,
        brightness: float | tuple[float, float],
        contrast: float | tuple[float, float],
        gamma: float | tuple[float, float],
        gain: float | tuple[float, float],
        gaussian_noise: dict[str, float],
        motion_blur: dict[str, int | float],
        sharpness: float,
        brightness_prob: float = 0.7,
        contrast_prob: float = 0.7,
        gamma_prob: float = 0.7,
        noise_prob: float = 0.7,
        motion_blur_prob: float = 0.7,
        sharpness_prob: float = 0.7,
    ) -> None:
        super().__init__()

        self.image_aug = AugmentationSequential(
            RandomBrightness(brightness=brightness, p=brightness_prob),
            RandomContrast(contrast=contrast, p=contrast_prob),
            RandomGaussianNoise(**gaussian_noise, p=noise_prob),
            RandomMotionBlur(**motion_blur, p=motion_blur_prob),
            RandomSharpness(sharpness=sharpness, p=sharpness_prob),
        )

    def forward(self, batch: dict[str, Tensor]) -> dict[str, Tensor]:
        batch["image0"] = self.image_aug(batch["image0"])
        batch["image1"] = self.image_aug(batch["image1"])

        return batch
