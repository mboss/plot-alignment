from collections.abc import Sequence
from typing import Any, cast

import torch
from kornia.augmentation import (
    AugmentationSequential,
    RandomPerspective,
    RandomResizedCrop,
    RandomRotation,
)
from kornia.geometry import crop_and_resize
from kornia.geometry.bbox import bbox_generator
from loguru import logger
from torch import Tensor, vmap
from torch.nn import Module
from torch.nn.functional import interpolate
from torchvision.transforms.functional import get_image_size

from plotalignment.transforms.functional import (
    get_corners,
    sort_corners,
    transform_points_normalized,
)
from plotalignment.utils import to_tuple

from .utils import calculate_patch_covisibility


class RandomHomographyPatch(Module):
    def __init__(
        self,
        image_size: int | tuple[int, int],
        crop_size: int | tuple[int, int],
        distortion_scale: float,
        degrees: float | tuple[float, float],
        perspective_probability: float,
        rotation_probability: float,
        first_crop_scale: tuple[float, float],
        second_crop_scale: tuple[float, float],
    ) -> None:
        super().__init__()

        self.crop_size_t = to_tuple(crop_size)
        self.augs = [
            AugmentationSequential(
                RandomPerspective(
                    distortion_scale=distortion_scale,
                    align_corners=True,
                    p=perspective_probability,
                    sampling_method="area_preserving",
                ),
                RandomRotation(degrees=degrees, p=rotation_probability),
                RandomResizedCrop(
                    size=self.crop_size_t,
                    scale=first_crop_scale,
                    cropping_mode="resample",
                ),
                data_keys=["image", "keypoints"],
            ),
            AugmentationSequential(
                RandomPerspective(
                    distortion_scale=distortion_scale,
                    align_corners=True,
                    p=perspective_probability,
                    sampling_method="area_preserving",
                ),
                RandomRotation(degrees=degrees, p=rotation_probability),
                RandomResizedCrop(
                    size=self.crop_size_t,
                    scale=second_crop_scale,
                    cropping_mode="resample",
                ),
                data_keys=["image", "keypoints"],
            ),
        ]

    def forward(self, batch: dict[str, Any]) -> dict[str, Any]:
        if torch.rand(1) < 0.5:
            batch["image0"], batch["image1"] = batch["image1"], batch["image0"]
            batch["hom_0to1"], batch["hom_1to0"] = (
                batch["hom_1to0"],
                batch["hom_0to1"],
            )
            if "points0" in batch:
                batch["points0"], batch["points1"] = batch["points1"], batch["points0"]
            batch["scale0"], batch["scale1"] = batch["scale1"], batch["scale0"]

        self.reversed = False

        image0, image1 = batch["image0"], batch["image1"]
        scale0, scale1, hom_0to1 = batch["scale0"], batch["scale1"], batch["hom_0to1"]
        W0, H0 = get_image_size(image0)
        W1, H1 = get_image_size(image1)
        N = image0.shape[0]

        image_a1: Tensor | None = None
        max_tries = 5
        for _ in range(max_tries):
            image_a0 = cast(
                Tensor, self.augment(image0.double(), index=0, data_keys=["image"])
            )
            w0, h0 = get_image_size(image_a0)
            mask_a0 = image_a0 != 0
            if (mask_a0.sum() / (N * w0 * h0)) < 0.5:
                continue

            corners0 = get_corners(image_a0).double()
            corners_r0 = cast(
                Tensor, self.inverse_augment(corners0, index=0, data_keys=["keypoints"])
            )
            if "hom_0to1" in batch:
                corners_w0 = corners_r0 * scale0[:, None]
                corners_w0 = transform_points_normalized(
                    corners_w0, hom_0to1, batch["camera_matrix"]
                )
                corners_w0 = corners_w0 / scale1[:, None]

            x_min, x_max = corners_w0[..., 0].aminmax(dim=1)
            y_min, y_max = corners_w0[..., 1].aminmax(dim=1)
            x_min, y_min = x_min.floor().clamp(0, W1), y_min.floor().clamp(0, H1)
            x_max, y_max = x_max.ceil().clamp(0, W1), y_max.ceil().clamp(0, H1)
            width, height = x_max - x_min, y_max - y_min

            sample_size = torch.as_tensor((h0, w0), device=corners0.device) * 1.2
            width_correction = width < sample_size[1]
            if width_correction.any():
                difference = sample_size[1] - width[width_correction]
                x_min[width_correction] = (
                    (x_min[width_correction] - difference / 2).floor().int()
                )
                x_max[width_correction] = (
                    (x_max[width_correction] + difference / 2).ceil().int()
                )
                width[width_correction] = (
                    x_max[width_correction] - x_min[width_correction]
                )
            height_correction = height < sample_size[0]
            if height_correction.any():
                difference = sample_size[0] - height[height_correction]
                y_min[height_correction] = (
                    (y_min[height_correction] - difference / 2).floor().int()
                )
                y_max[height_correction] = (
                    (y_max[height_correction] + difference / 2).ceil().int()
                )
                height[height_correction] = (
                    y_max[height_correction] - y_min[height_correction]
                )

            self.box1 = bbox_generator(x_min, y_min, width, height).int()

            image_a1_l = [
                crop_and_resize(
                    img[None], box[None], size=(height, width), align_corners=True
                )[0]
                for img, box in zip(image1.double(), self.box1, strict=True)
            ]

            for _ in range(2):
                image_a1_lt = self.augment(
                    *image_a1_l, index=1, data_keys=["image"] * len(image_a1_l)
                )
                if isinstance(image_a1_lt, list):
                    image_a1 = torch.cat(image_a1_lt)
                else:
                    image_a1 = image_a1_lt

                w1, h1 = get_image_size(image_a1)
                mask_a1 = image_a1 != 0
                if (mask_a1.sum() / (N * w1 * h1)) >= 0.5:
                    break

            corners1 = get_corners(image_a1).double()
            corners_r1 = cast(
                Tensor, self.inverse_augment(corners1, index=1, data_keys=["keypoints"])
            )

            corners_w0 = vmap(sort_corners)(corners_w0)
            corners_r1 = vmap(sort_corners)(corners_r1)

            try:
                covisibilities = calculate_patch_covisibility(corners_w0, corners_r1)
                if covisibilities.min() > 0.2:
                    break
            except Exception:
                continue

        coarse_scale = (
            batch["coarse_scale"]
            if isinstance(batch["coarse_scale"], int)
            else batch["coarse_scale"][0]
        )
        w_coarse0, h_coarse0 = w0 // coarse_scale, h0 // coarse_scale

        if image_a1 is None:
            logger.warning("Could not find a valid patch pair")
            logger.warning(f"path_image0: {batch['path_image0']}")
            logger.warning(f"path_image1: {batch['path_image1']}")

            batch["image0"] = image_a0.clone()
            batch["image1"] = image_a0.clone()
            self.augs[1] = self.augs[0]
            batch["aug"] = self
            zero_t = torch.zeros((N,))
            self.box1 = bbox_generator(zero_t, zero_t, zero_t + 100, zero_t + 100).int()
            batch["mask0"] = torch.zeros((N, h_coarse0, w_coarse0), dtype=torch.bool)
            batch["mask1"] = torch.zeros((N, h_coarse0, w_coarse0), dtype=torch.bool)
            return batch

        w_coarse1, h_coarse1 = w1 // coarse_scale, h1 // coarse_scale
        mask_a0 = (
            interpolate(
                mask_a0.float(), size=(h_coarse0, w_coarse0), mode="nearest-exact"
            )
            .bool()
            .squeeze(1)
        )
        mask_a1 = (
            interpolate(
                mask_a1.float(), size=(h_coarse1, w_coarse1), mode="nearest-exact"
            )
            .bool()
            .squeeze(1)
        )

        batch["image0"] = image_a0.float()
        batch["image1"] = image_a1.float()
        batch["aug"] = self

        batch["mask0"] = mask_a0
        batch["mask1"] = mask_a1

        if torch.rand(1) < 0.5:
            self.reversed = True
            batch["image0"], batch["image1"] = batch["image1"], batch["image0"]
            batch["mask0"], batch["mask1"] = batch["mask1"], batch["mask0"]
            batch["hom_0to1"], batch["hom_1to0"] = (
                batch["hom_1to0"],
                batch["hom_0to1"],
            )
            if "points0" in batch:
                batch["points0"], batch["points1"] = batch["points1"], batch["points0"]
            batch["scale0"], batch["scale1"] = batch["scale1"], batch["scale0"]

        return batch

    def augment(
        self,
        *data: Tensor,
        index: int,
        data_keys: Sequence[str],
        use_params: bool = False,
    ) -> Tensor | list[Tensor]:
        if self.reversed:
            index = 1 - index

        data_l = list(data)

        if index == 1:
            for i, key in enumerate(data_keys):
                if key == "keypoints":
                    data_l[i] = data_l[i] - self.box1[:, :1]

        if len(data_l) == 1:
            sample = self.augs[index](
                data_l[0],
                data_keys=data_keys,
                params=self.augs[index]._params if use_params else None,  # noqa
            )

            return cast(Tensor, sample)

        data_l = self.augs[index](
            *data_l,
            data_keys=data_keys,
            params=self.augs[index]._params if use_params else None,  # noqa
        )

        return data_l

    def inverse_augment(
        self, *data: Tensor, index: int, data_keys: list[str]
    ) -> Tensor | list[Tensor]:
        if self.reversed:
            index = 1 - index

        data_l = list(data)

        if len(data_l) == 1:
            sample = self.augs[index].inverse(data_l[0], data_keys=data_keys)
            if index == 1 and data_keys[0] == "keypoints":
                sample = sample + self.box1[:, :1]
            return cast(Tensor, sample)

        data_l = self.augs[index].inverse(*data_l, data_keys=data_keys)  # type: ignore

        if index == 1:
            for i, key in enumerate(data_keys):
                if key == "keypoints":
                    data_l[i] = data_l[i] + self.box1[:, :1]

        return data_l
