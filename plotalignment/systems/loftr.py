"""LoFTR module for PyTorch Lightning."""
from collections.abc import Callable
from copy import deepcopy
from pathlib import Path
from typing import Any

import dill as pickle
import kornia as K
import torch
from loguru import logger
from omegaconf import DictConfig
from pytorch_lightning import LightningModule
from torch import Tensor
from torch.nn import Module
from torch.optim import Optimizer
from torch.optim.lr_scheduler import LRScheduler
from torchvision.transforms.functional import resize

import wandb
from plotalignment.triangulation import b_ids_to_batch
from plotalignment.utils import draw_points, visualize_matches, visualize_predictions

from .utils import save_matches, spvs_fine

pickle.extend(use_dill=False)


class LoFTRModule(LightningModule):
    def __init__(
        self,
        model: Module,
        loss: Module,
        optimizer_partial: Callable[..., Optimizer],
        scheduler_partial: Callable[..., LRScheduler],
        cfg: "DictConfig | dict[str, Any]",
        **kwargs: Any,
    ) -> None:
        """Initialize the LoFTR module.

        Args:
            model: The model configuration.
            loss: The loss configuration.
            optimizer: The optimizer configuration.
            scheduler: The scheduler configuration.
            system: The system configuration.
            augmentation: The augmentation configuration.
        """
        super().__init__()
        self.save_hyperparameters(
            ignore=["model", "loss", "optimizer_partial", "scheduler_partial"]
        )

        self.optimizer_partial = optimizer_partial
        self.scheduler_partial = scheduler_partial
        self.loss = loss
        self.cfg = cfg
        self.model = self.setup_model(model)

    def setup_model(self, model: Module) -> Module:
        if "pretrained_ckpt" in self.cfg and self.cfg["pretrained_ckpt"] is not None:
            ckpt_path = Path(self.cfg["pretrained_ckpt"]).expanduser().resolve()
            state_dict = torch.load(ckpt_path, map_location="cpu")["state_dict"]
            if next(iter(state_dict.keys())).split(".")[0] == "model":
                state_dict = {k[6:]: v for k, v in state_dict.items()}
            model.load_state_dict(state_dict, strict=False)
            logger.success(f"Loaded pretrained model from {ckpt_path}.")

        if self.cfg.get("channels_last", False):
            model = model.to(memory_format=torch.channels_last)  # type: ignore

        if self.cfg.get("compile", False):
            model = torch.compile(model)  # type: ignore

        return model

    def configure_optimizers(self) -> dict[str, Any]:
        """Initialize the optimizer and learning rate scheduler.

        Returns: The optimizer and learning rate scheduler as a dictionary.
        """
        if self.trainer.model is None:
            return {}

        effective_batch_size = (
            self.cfg["train_batch_size"]
            * self.trainer.accumulate_grad_batches
            * self.trainer.num_devices
        )
        actual_lr = self.cfg["base_lr"] * effective_batch_size / 256

        optimizer = self.optimizer_partial(
            params=self.trainer.model.parameters(), lr=actual_lr
        )
        scheduler = self.scheduler_partial(
            optimizer=optimizer,
            num_training_steps=self.trainer.estimated_stepping_batches,
        )

        return {
            "optimizer": optimizer,
            "lr_scheduler": {
                "scheduler": scheduler,
                "interval": "step",
                "frequency": 1,
            },
        }

    def forward(self, batch: dict[str, Any]) -> dict[str, Any]:
        self.model(batch)
        return batch

    def _shared_step(
        self, batch: dict[str, Any], stage: str = "train"
    ) -> dict[str, Any]:
        batch = self.forward(batch)

        with torch.no_grad():
            batch = spvs_fine(
                batch,
                scale=self.cfg["feature_scale"][1],
                fine_window_size=self.cfg["fine_window_size"],
            )

        batch = self.loss(batch)

        return batch

    def training_step(
        self, batch: dict[str, Any], batch_idx: int, dataloader_idx: int = 0
    ) -> Any:
        batch = self._shared_step(batch, "train")

        with torch.no_grad():
            self.log_metrics(batch, "train")
            if (
                self.global_rank == 0
                and batch_idx % self.cfg.get("train_visualization_frequency", 500) == 0
            ):
                self.log_visualizations(batch, "train")

        return batch

    def validation_step(
        self, batch: dict[str, Any], batch_idx: int, dataloader_idx: int = 0
    ) -> Any:
        batch = self._shared_step(batch, "val")

        self.log_metrics(batch, "val")
        if (
            self.global_rank == 0
            and batch_idx % self.cfg.get("val_visualization_frequency", 50) == 0
        ):
            self.log_visualizations(batch, "val")

    def test_step(
        self, batch: dict[str, Any], batch_idx: int, dataloader_idx: int = 0
    ) -> Any:
        batch = self._shared_step(batch, "test")
        points0, points1 = batch["points0"], batch["points1"]

        self.log_metrics(batch, "test")

        mkpts0, mkpts1 = process_predictions(batch)

        from plotalignment.triangulation import find_plot_corners_with_essential

        points_w0, _ = find_plot_corners_with_essential(mkpts0, mkpts1, batch)

        transfer_error_max = float("nan")
        transfer_error_mean = float("nan")
        if len(points1) > 0:
            transfer_error_max = (points1 - points_w0).norm(dim=-1).max()
            transfer_error_mean = (points1 - points_w0).norm(dim=-1).mean()

        self.log_dict(
            {
                "test_transfer_error_max": transfer_error_max,
                "test_transfer_error_mean": transfer_error_mean,
            },
            on_step=True,
            on_epoch=True,
            rank_zero_only=True,
        )

        if (
            self.global_rank == 0
            and batch_idx % self.cfg.get("test_visualization_frequency", 1) == 0
        ):
            self.log_visualizations(
                batch, "test", points0=points0, points1=points1, points_w0=points_w0
            )

    def predict_step(self, batch: Any, batch_idx: int, dataloader_idx: int = 0) -> None:
        # 1. Forward batch & batch_inv
        batch_inv = deepcopy(batch)
        (
            batch_inv["image0"],
            batch_inv["image1"],
            batch_inv["scale0"],
            batch_inv["scale1"],
        ) = (batch["image1"], batch["image0"], batch["scale1"], batch["scale0"])
        if "mask0" in batch_inv:
            batch_inv["mask0"], batch_inv["mask1"] = (batch["mask1"], batch["mask0"])
        batch_inv = self.forward(batch_inv)
        batch_inv = {
            "mkpts0_f": batch_inv["mkpts1_f"],
            "mkpts1_f": batch_inv["mkpts0_f"],
            "m_bids": batch_inv["m_bids"],
            "mconf": batch_inv["mconf"],
            "bs": batch_inv["bs"],
            "camera_matrix": batch_inv["camera_matrix"],
            "plot_corners": batch_inv["plot_corners"],
            "polygon": batch_inv["polygon"],
        }
        batch = self.forward(batch)

        # 2. Get nested mkpts
        mkpts0, mkpts1 = process_predictions(
            batch, num_matches_used=self.cfg["num_matches_used"]
        )
        mkpts0_inv, mkpts1_inv = process_predictions(
            batch_inv, num_matches_used=self.cfg["num_matches_used"]
        )

        # 3. Save values
        predictions = {
            "mkpts0": mkpts0,
            "mkpts1": mkpts1,
            "mkpts0_inv": mkpts0_inv,
            "mkpts1_inv": mkpts1_inv,
            "yearly_experiment_number": batch["yearly_experiment_number"],
            "plot_uid": batch["plot_uid"],
            "date0": batch["date0"],
            "date1": batch["date1"],
            "time0": batch["time0"],
            "time1": batch["time1"],
        }
        for i in range(batch["bs"]):
            save_matches(
                predictions, index=i, save_path=Path(self.cfg["save_path_matches"])
            )

    @torch.no_grad()
    def log_metrics(
        self, batch: dict[str, Any], stage: str, num_patches: int = 0
    ) -> None:
        metrics = {
            f"{stage}_loss": batch["loss_scalars"]["loss"],
            f"{stage}_loss_coarse": batch["loss_scalars"]["loss_c"],
            f"{stage}_loss_fine": batch["loss_scalars"]["loss_f"],
        }
        if stage == "train" and self.cfg.get("homography_loss", False):
            metrics |= {f"{stage}_loss_hom": batch["loss_scalars"]["loss_hom"]}

        self.log_dict(
            metrics, on_step=True, on_epoch=stage != "train", sync_dist=stage != "train"
        )

    @torch.no_grad()
    def log_visualizations(
        self,
        batch: dict[str, Any],
        stage: str,
        index: int | None = None,
        points0: Tensor | None = None,
        points1: Tensor | None = None,
        points_w0: Tensor | None = None,
    ) -> None:
        if index is None:
            index = 0
            suffix = ""
        else:
            suffix = f"_{index}"

        if self.logger is not None and hasattr(self.logger.experiment, "log"):
            image0 = batch["image0"][index].clone()
            image1 = batch["image1"][index].clone()

            visualizations: dict[str, Tensor] = {}
            if "mask0" in batch:
                visualizations |= {
                    "mask0": batch["mask0"][index],
                    "mask1": batch["mask1"][index],
                }

            image_r0, image_r1 = self.resize_images_for_visualization(
                image0, image1, batch["scale0"], batch["scale1"], stage, index
            )
            image_rgb0 = K.color.grayscale_to_rgb(image_r0)
            image_rgb1 = K.color.grayscale_to_rgb(image_r1)

            visualizations |= self.create_visualizations(
                image_rgb0, image_rgb1, batch, stage, index
            )

            if stage == "test" and len(points_w0) > 0:
                visualizations |= self.draw_warped_points(
                    image_rgb0,
                    image_rgb1,
                    points0[index],
                    points1[index],
                    points_w0[index],
                )

            visualizations = {
                f"{key}{suffix}": wandb.Image(K.utils.tensor_to_image(value))
                for key, value in visualizations.items()
            }

            self.logger.experiment.log({stage: visualizations})

    @torch.no_grad()
    def draw_warped_points(self, image0, image1, points0, points1, points_w0):
        image_p0 = draw_points(image0, points0, color=(0, 255, 0))

        image_p1 = draw_points(image1, points1, color=(0, 255, 0))
        image_p1 = draw_points(image_p1, points_w0, color=(0, 0, 255))

        return {"points0": image_p0, "points1": image_p1}

    @torch.no_grad()
    def resize_images_for_visualization(
        self,
        image0: Tensor,
        image1: Tensor,
        scale0: Tensor,
        scale1: Tensor,
        stage: str,
        index: int = 0,
    ) -> tuple[Tensor, Tensor]:
        image_shape0 = (
            (
                scale0[index][[1, 0]]
                * torch.as_tensor(image0.shape[-2:], device=image0.device)
            )
            .int()
            .tolist()
        )
        image_shape1 = (
            (
                scale1[index][[1, 0]]
                * torch.as_tensor(image1.shape[-2:], device=image1.device)
            )
            .int()
            .tolist()
        )
        image_r0 = resize(image0, image_shape0)
        image_r1 = resize(image1, image_shape1)

        return image_r0, image_r1

    @torch.no_grad()
    def create_visualizations(
        self,
        image0: Tensor,
        image1: Tensor,
        batch: dict[str, Any],
        stage: str,
        index: int = 0,
    ) -> dict[str, Any]:
        visualizations: dict[str, Tensor] = {}
        if batch["mkpts0_c"].numel() > 0:
            visualizations |= visualize_predictions(batch, image0, image1, index)
            visualizations |= visualize_matches(
                batch, image0.clone(), image1.clone(), index
            )
        else:
            visualizations |= {"image_pfc0": image0, "image_pfc1": image1}

        return visualizations

    def on_after_batch_transfer(
        self, batch: dict[str, Any], dataloader_idx: int
    ) -> dict[str, Any]:
        if self.cfg.get("channels_last", False):
            for key, value in batch.items():
                if isinstance(value, Tensor) and len(value.shape) == 4:
                    batch[key] = value.to(memory_format=torch.channels_last)  # type: ignore
        return batch


def batch_to_nested(batch: dict[str, Any]) -> dict[str, Any]:
    mkpts0, b_ids = b_ids_to_batch(batch["m_bids"], batch["mkpts0_f"])
    mkpts1, _ = b_ids_to_batch(batch["m_bids"], batch["mkpts1_f"])
    mconf, _ = b_ids_to_batch(batch["m_bids"], batch["mconf"])

    if b_ids.numel() != batch["bs"]:
        mkpts0_partial = mkpts0
        mkpts1_partial = mkpts1
        mconf_partial = mconf

        mkpts0_l = [
            torch.empty((0, 2), device=mkpts0_partial.device)
            for _ in range(batch["bs"])
        ]
        mkpts1_l = [
            torch.empty((0, 2), device=mkpts0_partial.device)
            for _ in range(batch["bs"])
        ]
        mconf_l = [
            torch.empty((0,), device=mkpts0_partial.device) for _ in range(batch["bs"])
        ]
        for i, b_id in enumerate(b_ids):
            mkpts0_l[b_id] = mkpts0_partial[i]
            mkpts1_l[b_id] = mkpts1_partial[i]
            mconf_l[b_id] = mconf_partial[i]

        mkpts0 = torch.nested.as_nested_tensor(mkpts0_l)
        mkpts1 = torch.nested.as_nested_tensor(mkpts1_l)
        mconf = torch.nested.as_nested_tensor(mconf_l)

    return {"mkpts0": mkpts0, "mkpts1": mkpts1, "mconf": mconf, "b_ids": b_ids}


def process_predictions(
    batch: dict[str, Any], num_matches_used: int = 2500
) -> tuple[Tensor, Tensor]:
    batch_nested = batch_to_nested(batch)
    mkpts0, mkpts1, mconf = (
        batch_nested["mkpts0"],
        batch_nested["mkpts1"],
        batch_nested["mconf"],
    )

    any_to_shorten = any(len(mkpt0) > num_matches_used for mkpt0 in mkpts0)
    if any_to_shorten:
        new_mkpts0 = []
        new_mkpts1 = []

        for i, conf in enumerate(mconf):
            if len(mkpts0[i]) > num_matches_used:
                top_indices = conf.argsort(descending=True)[:num_matches_used]
                new_mkpts0.append(mkpts0[i][top_indices])
                new_mkpts1.append(mkpts1[i][top_indices])
            else:
                new_mkpts0.append(mkpts0[i])
                new_mkpts1.append(mkpts1[i])

        mkpts0 = torch.nested.as_nested_tensor(new_mkpts0)
        mkpts1 = torch.nested.as_nested_tensor(new_mkpts1)

    return mkpts0, mkpts1
