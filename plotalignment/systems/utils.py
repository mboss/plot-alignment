from datetime import datetime
from operator import itemgetter
from pathlib import Path
from typing import Any, cast

import dill as pickle
import geopandas as gpd
import torch
from einops import repeat
from kornia.feature import match_smnn
from kornia.utils import create_meshgrid
from loguru import logger
from shapely import Polygon
from torch import Tensor

from plotalignment.transforms.functional import (
    check_angles,
    transform_points_normalized,
)
from plotalignment.triangulation import find_plot_corners_with_essential


def mask_pts_at_padded_regions(grid_pt, mask):
    """For megadepth dataset, zero-padding exists in images"""
    mask = repeat(mask, "n h w -> n (h w) c", c=2)
    grid_pt[~mask.bool()] = 0
    return grid_pt


def out_bound_mask(pt, w, h):
    return (pt[..., 0] < 0) + (pt[..., 0] >= w) + (pt[..., 1] < 0) + (pt[..., 1] >= h)


def spvs_coarse(batch: dict[str, Any], scale: int) -> dict[str, Any]:
    """Update:
        data (dict): {
            "conf_matrix_gt": [N, hw0, hw1],
            'spv_b_ids': [M]
            'spv_i_ids': [M]
            'spv_j_ids': [M]
            'spv_w_pt0_i': [N, hw0, 2], in original image resolution
            'spv_pt1_i': [N, hw1, 2], in original image resolution
        }.

    Note:
        - for scannet dataset, there're 3 kinds of resolution {i, c, f}
        - for megadepth dataset, there're 4 kinds of resolution {i, i_resize, c, f}
    """
    # 1. misc
    image0, image1 = cast(Tensor, batch["image0"]), cast(Tensor, batch["image1"])
    device = image0.device
    N, _, H0, W0 = image0.shape
    _, _, H1, W1 = image1.shape
    h0, w0, h1, w1 = (x // scale for x in [H0, W0, H1, W1])

    scale0 = torch.as_tensor(scale, device=device)
    if "scale0" in batch:
        scale0 = scale0 * batch["scale0"]
        scale0 = scale0[:, None]

    scale1 = torch.as_tensor(scale, device=device)
    if "scale1" in batch:
        scale1 = scale1 * batch["scale1"]
        scale1 = scale1[:, None]

    # 2. warp grids
    # create kpts in meshgrid and resize them to image resolution
    grid_pt0_c = (
        create_meshgrid(h0, w0, False, device, dtype=torch.float64)
        .reshape(1, h0 * w0, 2)
        .repeat(N, 1, 1)
    )  # [N, hw, 2]
    grid_pt1_c = (
        create_meshgrid(h1, w1, False, device, dtype=torch.float64)
        .reshape(1, h1 * w1, 2)
        .repeat(N, 1, 1)
    )
    grid_pt0_i = scale0 * grid_pt0_c
    grid_pt1_i = scale1 * grid_pt1_c

    # mask padded region to (0, 0), so no need to manually mask conf_matrix_gt
    if "mask0" in batch:
        grid_pt0_i = mask_pts_at_padded_regions(grid_pt0_i, batch["mask0"])
        grid_pt1_i = mask_pts_at_padded_regions(grid_pt1_i, batch["mask1"])

    if "aug" in batch:
        grid_pt0_i = grid_pt0_c * scale
        grid_pt1_i = grid_pt1_c * scale

        grid_pt0_i_orig = grid_pt0_i.clone()
        grid_pt1_i_orig = grid_pt1_i.clone()

        grid_pt0_i = batch["aug"].inverse_augment(
            grid_pt0_i, index=0, data_keys=["keypoints"]
        )
        grid_pt1_i = batch["aug"].inverse_augment(
            grid_pt1_i, index=1, data_keys=["keypoints"]
        )

        grid_pt0_i = grid_pt0_i * batch["scale0"].unsqueeze(1)
        grid_pt1_i = grid_pt1_i * batch["scale1"].unsqueeze(1)

    if "hom_0to1" in batch:
        w_pt0_i = transform_points_normalized(
            grid_pt0_i, batch["hom_0to1"], batch["camera_matrix"]
        )
        w_pt1_i = transform_points_normalized(
            grid_pt1_i, batch["hom_1to0"], batch["camera_matrix"]
        )

    if "aug" in batch:
        if "hom_0to1" not in batch:
            w_pt0_i = grid_pt0_i.clone()
            w_pt1_i = grid_pt1_i.clone()

        grid_pt0_i = grid_pt0_i_orig
        grid_pt1_i = grid_pt1_i_orig

        w_pt0_i = w_pt0_i / batch["scale1"].unsqueeze(1)
        w_pt1_i = w_pt1_i / batch["scale0"].unsqueeze(1)

        w_pt0_i = batch["aug"].augment(
            w_pt0_i, index=1, data_keys=["keypoints"], use_params=True
        )
        w_pt1_i = batch["aug"].augment(
            w_pt1_i, index=0, data_keys=["keypoints"], use_params=True
        )

        grid_pt0_i = grid_pt0_i * batch["scale0"].unsqueeze(1)
        grid_pt1_i = grid_pt1_i * batch["scale1"].unsqueeze(1)
        w_pt0_i = w_pt0_i * batch["scale1"].unsqueeze(1)
        w_pt1_i = w_pt1_i * batch["scale0"].unsqueeze(1)

    batch["grid_pt0_i"] = grid_pt0_i.float().detach().clone()
    batch["grid_pt1_i"] = grid_pt1_i.float().detach().clone()
    batch["w_pt0_i"] = w_pt0_i.float().detach().clone()
    batch["w_pt1_i"] = w_pt1_i.float().detach().clone()

    # 2. compute mutual nearest neighbor
    b_ids_list, i_ids_list, j_ids_list = [], [], []
    scale_w0, scale_h0 = batch["scale0"].permute(1, 0)
    scale_w1, scale_h1 = batch["scale1"].permute(1, 0)
    for i, (pt0_i, pt1_i) in enumerate(
        zip(w_pt0_i.clone(), grid_pt1_i.clone(), strict=True)
    ):
        pt0_i[out_bound_mask(pt0_i, W1 * scale_w1[i], H1 * scale_h1[i])] = 0
        pt1_i[out_bound_mask(pt1_i, W0 * scale_w0[i], H0 * scale_h0[i])] = 0

        matches = match_smnn(pt0_i, pt1_i)[1]
        i_ids_list.append(matches[:, 0])
        j_ids_list.append(matches[:, 1])
        b_ids_list.append(torch.full_like(matches[:, 0], i))

    b_ids = torch.cat(b_ids_list)
    i_ids = torch.cat(i_ids_list)
    j_ids = torch.cat(j_ids_list)

    conf_matrix_gt = torch.zeros(N, h0 * w0, h1 * w1, device=device)
    conf_matrix_gt[b_ids, i_ids, j_ids] = 1
    batch.update({"conf_matrix_gt": conf_matrix_gt})

    # 5. save coarse matches(gt) for training fine level
    if len(b_ids) == 0:
        logger.warning(
            "No groundtruth coarse match found for: "
            f"{itemgetter('yearly_experiment_number', 'plot_uid')(batch)}"
        )
        # this won't affect fine-level loss calculation
        b_ids = torch.tensor([0], device=device)
        i_ids = torch.tensor([0], device=device)
        j_ids = torch.tensor([0], device=device)

    batch.update({"spv_b_ids": b_ids, "spv_i_ids": i_ids, "spv_j_ids": j_ids})

    # 6. save intermediate results (for fast fine-level computation)
    batch.update({"spv_w_pt0_i": w_pt0_i.float(), "spv_pt1_i": grid_pt1_i.float()})

    return batch


def spvs_fine(
    batch: dict[str, Any], scale: int, fine_window_size: int
) -> dict[str, Any]:
    """Update:
    data (dict):{
    "expec_f_gt": [M, 2]}.
    """
    # 1. misc
    w_pt0_i, pt1_i = batch.pop("spv_w_pt0_i"), batch.pop("spv_pt1_i")
    # w_pt0_i, pt1_i = batch["spv_w_pt0_i"], batch["spv_pt1_i"]
    radius = fine_window_size // 2

    # 2. get coarse prediction
    b_ids, i_ids, j_ids = batch["b_ids"], batch["i_ids"], batch["j_ids"]

    # 3. compute gt
    scale = scale * batch["scale1"][b_ids] if "scale0" in batch else scale
    # `expec_f_gt` might exceed the window,
    # i.e. abs(*) > 1, which would be filtered later
    batch["expec_f_gt"] = (
        (w_pt0_i[b_ids, i_ids] - pt1_i[b_ids, j_ids]) / scale / radius
    )  # [M, 2]

    return batch


def predict_with_matches(
    batch: dict[str, Any],
    check_parameters: dict[str, Any],
) -> None:
    if check_parameters["corner_check_parameters"] is None:
        check_parameters["corner_check_parameters"] = {}

    batch["bs"] = len(batch["mkpts0_f"])
    mkpts0, mkpts1 = (
        torch.nested.as_nested_tensor(
            [pts[~pts.isnan().any(1)] for pts in batch["mkpts0_f"]]
        ),
        torch.nested.as_nested_tensor(
            [pts[~pts.isnan().any(1)] for pts in batch["mkpts1_f"]]
        ),
    )
    mkpts0_inv, mkpts1_inv = (
        torch.nested.as_nested_tensor(
            [pts[~pts.isnan().any(1)] for pts in batch["mkpts0_f_inv"]]
        ),
        torch.nested.as_nested_tensor(
            [pts[~pts.isnan().any(1)] for pts in batch["mkpts1_f_inv"]]
        ),
    )
    mkpts0_combined, mkpts1_combined = (
        torch.nested.as_nested_tensor(
            [
                torch.cat([mkpt0, mkpt0_inv])
                for mkpt0, mkpt0_inv in zip(mkpts0, mkpts0_inv)
            ]
        ),
        torch.nested.as_nested_tensor(
            [
                torch.cat([mkpt1, mkpt1_inv])
                for mkpt1, mkpt1_inv in zip(mkpts1, mkpts1_inv)
            ]
        ),
    )

    plot_corners1_stacked = torch.stack(
        [
            find_plot_corners_with_essential(
                pts0, pts1, batch, **check_parameters["corner_check_parameters"]
            )[0]
            for pts0, pts1 in (
                (mkpts0, mkpts1),
                (mkpts0_inv, mkpts1_inv),
                (mkpts0_combined, mkpts1_combined),
            )
        ],
        dim=1,
    )

    def get_closeness_masks(
        pts: Tensor,
        pts_reduced: Tensor,
        threshold_max_difference: float,
        threshold_mean_difference: float,
        threshold_std_difference: float,
    ) -> Tensor:
        pts_diff = (pts_reduced[:, None] - pts).norm(dim=-1)

        pts_diff_max_mask = pts_diff.max(dim=-1)[0] < threshold_max_difference
        pts_diff_mean_mask = pts_diff.mean(dim=-1) < threshold_mean_difference
        pts_diff_std_mask = pts_diff.std(dim=-1) < threshold_std_difference

        return (
            pts_diff_max_mask.all(dim=-1)
            & pts_diff_mean_mask.all(dim=-1)
            & pts_diff_std_mask.all(dim=-1)
        )

    plot_corners1_fully_correct_mask = get_closeness_masks(
        plot_corners1_stacked[:, :-1],
        plot_corners1_stacked[:, -1],
        check_parameters["threshold_max_difference_predictions"],
        check_parameters["threshold_mean_difference_predictions"],
        check_parameters["threshold_std_difference_predictions"],
    )

    plot_corners1_partially_correct_mask = get_closeness_masks(
        plot_corners1_stacked[:, :1],
        plot_corners1_stacked[:, 1],
        check_parameters["threshold_max_difference_predictions"],
        check_parameters["threshold_mean_difference_predictions"],
        check_parameters["threshold_std_difference_predictions"],
    )

    plot_corners1 = plot_corners1_stacked[:, -1]
    plot_corners1_partially_but_not_fully_correct_mask = (
        ~plot_corners1_fully_correct_mask & plot_corners1_partially_correct_mask
    )
    plot_corners1[
        plot_corners1_partially_but_not_fully_correct_mask
    ] = plot_corners1_stacked[:, :2].mean(dim=1)[
        plot_corners1_partially_but_not_fully_correct_mask
    ]

    plot_corners_correct_mask = (
        plot_corners1_fully_correct_mask | plot_corners1_partially_correct_mask
    )

    if not plot_corners_correct_mask.all():
        logger.warning("Corners did not match.")

    predictions = {
        "bs": batch["bs"],
        "plot_corners1": plot_corners1,
        "plot_corners_correct_mask": plot_corners_correct_mask,
        "image_names": [Path(path_image).stem for path_image in batch["path_image1"]],
        "yearly_experiment_number": batch["yearly_experiment_number"],
        "plot_uid": batch["plot_uid"],
        "date0": batch["date0"],
        "date1": batch["date1"],
        "time0": batch["time0"],
        "time1": batch["time1"],
        "num_steps": list(map(int, batch["num_steps"])),
        "check_name": check_parameters["name"],
    }

    return predictions


def predict_with_homography(batch: dict[str, Any]) -> dict[str, Any]:
    plot_corners0 = batch["polygon"]

    plot_corners1 = transform_points_normalized(
        plot_corners0, batch["hom_0to1"], batch["camera_matrix"]
    )
    plot_corners_w0 = transform_points_normalized(
        plot_corners1, batch["hom_1to0"], batch["camera_matrix"]
    )

    plot_corners1_correct_mask = (
        torch.isclose(plot_corners0, plot_corners_w0, atol=1e-3).all(dim=-1).all(dim=-1)
    )
    angles_in_bounds, _ = check_angles(plot_corners1)

    plot_corners1_correct_mask = plot_corners1_correct_mask & angles_in_bounds

    predictions = {
        "bs": len(plot_corners0),
        "plot_corners1": plot_corners1,
        "plot_corners_correct_mask": plot_corners1_correct_mask,
        "image_names": [Path(path_image).stem for path_image in batch["path_image1"]],
        "yearly_experiment_number": batch["yearly_experiment_number"],
        "plot_uid": batch["plot_uid"],
        "date0": batch["date0"],
        "date1": batch["date1"],
        "time0": batch["time0"],
        "time1": batch["time1"],
        "num_steps": list(map(int, batch["num_steps"])),
    }

    return predictions


def save_files(
    predictions: dict[str, Any],
    save_path_aligned: Path | None = None,
    save_path_unalignable: Path | None = None,
    save_path_potentially_correct: Path | None = None,
) -> bool:
    for i in range(predictions["bs"]):
        if "plot_corners_correct_mask" in predictions:
            if predictions["plot_corners_correct_mask"][i]:
                filepath_alignment = (
                    save_path_aligned / f"{predictions['image_names'][i]}.geojson"
                )
                save_alignment(
                    predictions["image_names"][i],
                    predictions["plot_corners1"][i],
                    filepath_alignment,
                    predictions["num_steps"][i] + 1,
                    predictions["check_name"],
                )
            else:
                if (
                    "hom_correct_mask" in predictions
                    and predictions["hom_correct_mask"][i]
                ):
                    save_homography(predictions, i, save_path_potentially_correct)

                save_unalignable(predictions, i, save_path_unalignable)

        return True


def save_unalignable(
    predictions: dict[str, Any], index: int, save_path_unalignable
) -> None:
    date0 = (
        datetime.strptime(predictions["date0"][index], "%Y-%m-%d")
        .date()
        .strftime("%Y%m%d")
    )
    date1 = (
        datetime.strptime(predictions["date1"][index], "%Y-%m-%d")
        .date()
        .strftime("%Y%m%d")
    )

    time0 = (
        datetime.strptime(predictions["time0"][index], "%H:%M:%S")
        .time()
        .strftime("%H%M%S")
    )
    time1 = (
        datetime.strptime(predictions["time1"][index], "%H:%M:%S")
        .time()
        .strftime("%H%M%S")
    )

    yearly_experiment_number = predictions["yearly_experiment_number"][index]
    plot_uid = predictions["plot_uid"][index]

    filepath_unalignable = (
        save_path_unalignable
        / f"FP{yearly_experiment_number}{plot_uid}_{date0}_{time0}_{date1}_{time1}"
    )
    filepath_unalignable.touch()


def save_alignment(
    image_name: str, polygon: Tensor, path: Path, num_steps: int, check_name: str
) -> None:
    alignment_path = f"{path.with_suffix('')}_{num_steps}_{check_name}{path.suffix}"
    alignment_df = gpd.GeoDataFrame(
        {"image": [image_name]}, geometry=[Polygon(polygon.cpu().numpy())]
    )
    alignment_df.to_file(f"{alignment_path}.tmp", driver="GeoJSON")
    Path(f"{alignment_path}.tmp").rename(alignment_path)


def save_matches(predictions: dict[str, Any], index: int, save_path: Path) -> None:
    mkpts_0to1 = {
        "mkpts0_f": predictions["mkpts0"][index].cpu(),
        "mkpts1_f": predictions["mkpts1"][index].cpu(),
        "mkpts0_f_inv": predictions["mkpts0_inv"][index].cpu(),
        "mkpts1_f_inv": predictions["mkpts1_inv"][index].cpu(),
    }

    date0 = (
        datetime.strptime(predictions["date0"][index], "%Y-%m-%d")
        .date()
        .strftime("%Y%m%d")
    )
    date1 = (
        datetime.strptime(predictions["date1"][index], "%Y-%m-%d")
        .date()
        .strftime("%Y%m%d")
    )

    time0 = (
        datetime.strptime(predictions["time0"][index], "%H:%M:%S")
        .time()
        .strftime("%H%M%S")
    )
    time1 = (
        datetime.strptime(predictions["time1"][index], "%H:%M:%S")
        .time()
        .strftime("%H%M%S")
    )

    yearly_experiment_number = predictions["yearly_experiment_number"][index]
    plot_uid = predictions["plot_uid"][index]

    path_matches_0to1 = (
        save_path
        / f"FP{yearly_experiment_number}{plot_uid}_{date0}_{time0}_{date1}_{time1}"
    )
    with Path(f"{path_matches_0to1}.tmp").open("wb") as f:
        pickle.dump(mkpts_0to1, f)
    Path(f"{path_matches_0to1}.tmp").rename(path_matches_0to1)


def save_homography(predictions: dict[str, Any], index: int, path: Path) -> None:
    path.mkdir(parents=True, exist_ok=True)

    date0 = (
        datetime.strptime(predictions["date0"][index], "%Y-%m-%d")
        .date()
        .strftime("%Y%m%d")
    )
    date1 = (
        datetime.strptime(predictions["date1"][index], "%Y-%m-%d")
        .date()
        .strftime("%Y%m%d")
    )

    time0 = (
        datetime.strptime(predictions["time0"][index], "%H:%M:%S")
        .time()
        .strftime("%H%M%S")
    )
    time1 = (
        datetime.strptime(predictions["time1"][index], "%H:%M:%S")
        .time()
        .strftime("%H%M%S")
    )

    yearly_experiment_number = predictions["yearly_experiment_number"][index]
    plot_uid = predictions["plot_uid"][index]
    num_steps = predictions["num_steps"][index]

    filepath_homography = (
        path
        / f"FP{yearly_experiment_number}{plot_uid}_{date0}_{time0}_{date1}_{time1}_{num_steps}"
    )

    with Path(f"{filepath_homography}.tmp").open("wb") as f:
        pickle.dump(predictions["hom_0to1"][index].cpu(), f)
    Path(f"{filepath_homography}.tmp").rename(filepath_homography)
