from pathlib import Path
from typing import Any

import hydra
from dask.distributed import Client
from hydra.utils import instantiate
from loguru import logger
from omegaconf import DictConfig
from torchvision.utils import save_image
from tqdm import tqdm

from plotalignment.utils import read_filenames_regex_pd


@hydra.main(config_path="conf", config_name="preprocess", version_base=None)
def main(cfg: DictConfig | dict[str, Any]) -> None:
    logger.remove(0)
    logger.add("logs/preprocess.log", mode="w")

    dm = instantiate(cfg["datamodule"])
    dm.prepare_data()
    dm.setup("predict")

    for sample in tqdm(dm.predict_dataloader()):
        save_path_image = Path(
            sample["path_image"].replace(cfg["drive_path"], cfg["preprocess_save_root"])
        )
        save_path_image = save_path_image.parent / (save_path_image.stem + ".png")

        save_path_image.parent.mkdir(parents=True, exist_ok=True)
        save_image(sample["image"], save_path_image)

    df_preprocessed = read_filenames_regex_pd(
        **dm.cfg_datapipes["remote"]["png"]["pipe"], path_column_name="path_image"
    )
    df_preprocessed.to_parquet(dm.cfg["save_path_preprocessed_images"])

    logger.success("All files preprocessed!")


if __name__ == "__main__":
    client = Client()
    print(client.dashboard_link)

    main()
