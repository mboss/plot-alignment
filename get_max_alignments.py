from typing import Any

import hydra
import pandas as pd
from omegaconf import DictConfig

from plotalignment.utils import filter_by_df, read_filenames_regex_pd


@hydra.main(config_path="conf", config_name="predict", version_base=None)
def main(cfg: DictConfig | dict[str, Any]) -> None:
    datapipes = cfg.datamodule.datapipe
    cfg_initial = datapipes.remote.initial
    df_initial = read_filenames_regex_pd(
        **cfg_initial.pipe, path_column_name="path_points"
    )
    df_initial = df_initial[
        df_initial["yearly_experiment_number"] == cfg.experiment_number
    ]

    df_images = pd.read_parquet("/home/mboss/data/fip_data/preprocessed_images.parquet")

    print(
        len(df_images)
        - len(
            filter_by_df(
                df_images, df_initial, ["yearly_experiment_number", "plot_uid"]
            )
        )
    )


if __name__ == "__main__":
    main()
