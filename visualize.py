import shutil
from pathlib import Path
from typing import TYPE_CHECKING, Any

import hydra
import imageio as iio
import torch
from hydra.utils import instantiate
from kornia.geometry import bbox_generator, get_perspective_transform, warp_perspective
from loguru import logger
from torchvision.transforms.functional import resize, to_pil_image
from torchvision.utils import save_image

from plotalignment.utils import draw_points

if TYPE_CHECKING:
    from omegaconf import DictConfig


@hydra.main(config_path="conf", config_name="visualize", version_base=None)
def main(cfg: "DictConfig | dict[str, Any]") -> None:
    logger.remove(0)
    logger.add("logs/visualize.log", mode="w")

    logger.info(
        f"Saving images of year {cfg['year']} and experiment {cfg['experiment_number']}."
    )
    image_folder = Path("images") / str(cfg["year"]) / str(cfg["experiment_number"])

    if "plot_uid" in cfg.datamodule.cfg:
        image_folder = image_folder / cfg.datamodule.cfg["plot_uid"]

    image_paths = [image_folder]

    for image_path in image_paths:
        if image_path.is_dir():
            shutil.rmtree(image_path)

        (image_path / "images").mkdir(parents=True)
        (image_path / "plot_gifs").mkdir(parents=True)
        (image_path / "image_gifs").mkdir(parents=True)

    dm_shortest = instantiate(cfg["datamodule"], sort_longest_first=True)

    for dm, image_path in zip([dm_shortest], image_paths, strict=True):
        dm.prepare_data()
        dm.setup("predict")

        for i, group in enumerate(dm.predict_dataloader()):
            if (
                "plot_uid" in cfg.datamodule.cfg
                and group["plot_uid"][0] != cfg.datamodule.cfg["plot_uid"]
            ):
                continue

            image_name = (
                f"{i}_{len(group['path_image'])}_{Path(group['path_image'][0]).stem}"
            )

            images = group["image"]
            polygon_paths = group["path_points"]

            def add_colored_border(input_tensor, border_size, border_color):
                """
                Adds a colored border to an image tensor without changing its size.

                :param input_tensor: Tensor of shape (C, H, W)
                :param border_size: Integer, size of the border
                :param border_color: Tuple (R, G, B), values between 0 and 1
                :return: Tensor with the border added
                """
                C, H, W = input_tensor.shape

                # Color the top and bottom borders
                input_tensor[:, :border_size, :] = torch.tensor(border_color).view(
                    C, 1, 1
                )
                input_tensor[:, H - border_size :, :] = torch.tensor(border_color).view(
                    C, 1, 1
                )

                # Color the left and right borders
                input_tensor[:, :, :border_size] = torch.tensor(border_color).view(
                    C, 1, 1
                )
                input_tensor[:, :, W - border_size :] = torch.tensor(border_color).view(
                    C, 1, 1
                )

                return input_tensor

            # Example usage
            # Assuming 'image_tensor' is your original image tensor of shape (C, H, W)
            colors = {
                "tight": (0, 255, 0),
                "medium": (0, 0, 255),
                "relaxed": (255, 0, 0),
                "loose": (255, 255, 0),
                "trash": (0, 255, 255),
                "FP": (255, 255, 255),
            }

            for i in range(len(images)):
                border_color = next(
                    colors[name] for name in colors if name in polygon_paths[i]
                )
                images[i] = add_colored_border(images[i], 10, border_color)

            polygons = group["polygon"]

            polygons = polygons / group["scale0"][0]

            imgs = []
            for image, polygon in zip(images, polygons, strict=True):
                image_p = draw_points(image, polygon, thickness=5, radius=10)
                image_p = resize(image_p, (900 // 2, 1500 // 2), antialias=True)
                imgs.append(image_p)
            imgs_t = torch.stack(imgs)
            save_image(imgs_t, f"{image_path}/images/{image_name}.jpg", nrow=4)

            num_images = len(images)

            plot_bboxes = bbox_generator(
                x_start=torch.zeros(num_images, dtype=torch.double),
                y_start=torch.zeros(num_images, dtype=torch.double),
                width=torch.full(
                    (num_images,), dm.cfg["image_size"][1], dtype=torch.double
                ),
                height=torch.full(
                    (num_images,), dm.cfg["image_size"][0], dtype=torch.double
                ),
            )

            # if len(images) > 1:
            #     images_w_l = []
            #     for image, polygon, plot_bbox in zip(
            #         images, polygons, plot_bboxes, strict=True
            #     ):
            #         transform = get_perspective_transform(
            #             polygon[None], plot_bbox[None]
            #         )
            #         image_w = warp_perspective(
            #             image[None].double(),
            #             transform,
            #             dm.cfg["image_size"],
            #             align_corners=True,
            #         )[0]
            #         images_w_l.append(image_w)

            #     iio.mimsave(
            #         f"{image_path}/plot_gifs/{image_name}.gif",
            #         [to_pil_image(image.float()) for image in images_w_l],
            #         duration=200,
            #     )

            if len(images) > 1:
                images_w_l = []
                for image, polygon, plot_bbox in zip(
                    images, polygons, plot_bboxes, strict=True
                ):
                    polygon_mean = polygon.mean(dim=0)
                    polygon = polygon - polygon_mean
                    polygon = polygon * 1.5
                    polygon = polygon + polygon_mean

                    transform = get_perspective_transform(
                        polygon[None], plot_bbox[None]
                    )
                    image_w = warp_perspective(
                        image[None].double(),
                        transform,
                        dm.cfg["image_size"],
                        align_corners=True,
                    )[0]
                    images_w_l.append(image_w)

                iio.mimsave(
                    f"{image_path}/image_gifs/{image_name}.gif",
                    [to_pil_image(image.float()) for image in images_w_l],
                    duration=200,
                )

            logger.success(f"Group {i} done.")

            if "plot_uid" in cfg.datamodule.cfg:
                break


if __name__ == "__main__":
    torch.set_float32_matmul_precision("medium")

    main()
