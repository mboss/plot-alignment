from pathlib import Path
from typing import Any

import hydra
import imageio as iio
import torch
from hydra.utils import instantiate
from kornia.geometry import bbox_generator, get_perspective_transform, warp_perspective
from loguru import logger
from omegaconf import DictConfig
from torchvision.transforms.functional import resize, to_pil_image
from torchvision.utils import save_image

from plotalignment.systems.utils import save_alignment
from plotalignment.transforms.functional import transform_points_normalized
from plotalignment.utils import draw_points


@hydra.main(
    config_path="conf", config_name="check_potentially_correct", version_base=None
)
def main(cfg: DictConfig | dict[str, Any]) -> None:
    logger.info(
        "Checking potentially correct homographies "
        f"of year {cfg['year']} and experiment {cfg['experiment_number']}."
    )

    image_path = Path("./images/potentially_correct")
    image_path.mkdir(exist_ok=True, parents=True)

    dm = instantiate(cfg["datamodule"])
    dm.prepare_data()
    dm.setup("predict")

    for batch in dm.predict_dataloader():
        images0 = batch["image0"]
        images1 = batch["image1"]
        images_initial = batch["image_initial"]
        plot_corners0 = batch["polygon"]
        plot_corners_initial = batch["polygon_initial"]
        homs_0to1 = batch["homography"]
        camera_matrix = batch["camera_matrix"]

        num_images = len(images0)
        image_size = batch["image_size"][0]
        image_size_original = batch["image_size_original"]
        scale = image_size_original / image_size

        plot_corners1 = transform_points_normalized(
            plot_corners0, homs_0to1, camera_matrix
        )

        plot_corners_r0 = plot_corners0 / scale
        plot_corners_r1 = plot_corners1 / scale
        plot_corners_initial = plot_corners_initial / scale

        images_r0 = resize(images0, image_size, antialias=True)
        images_r1 = resize(images1, image_size, antialias=True)
        images_initial_r = resize(images_initial, image_size, antialias=True)

        plot_bboxes = bbox_generator(
            x_start=torch.zeros(num_images, dtype=torch.double),
            y_start=torch.zeros(num_images, dtype=torch.double),
            width=torch.full((num_images,), image_size[1], dtype=torch.double),
            height=torch.full((num_images,), image_size[0], dtype=torch.double),
        )

        plot_corners_mean0 = plot_corners_r0.mean(dim=1)
        plot_corners_e0 = plot_corners_r0 - plot_corners_mean0
        plot_corners_e0 = plot_corners_e0 * 1.5
        plot_corners_e0 = plot_corners_e0 + plot_corners_mean0

        transform = get_perspective_transform(plot_corners_e0, plot_bboxes)
        images_w0 = warp_perspective(
            images_r0.double(), transform, image_size, align_corners=True
        )

        plot_corners_mean1 = plot_corners_r1.mean(dim=1)
        plot_corners_e1 = plot_corners_r1 - plot_corners_mean1
        plot_corners_e1 = plot_corners_e1 * 1.5
        plot_corners_e1 = plot_corners_e1 + plot_corners_mean1

        transform = get_perspective_transform(plot_corners_e1, plot_bboxes)
        images_w1 = warp_perspective(
            images_r1.double(), transform, image_size, align_corners=True
        )

        image_names = [Path(path_image).stem for path_image in batch["path_image1"]]
        save_path_aligned = Path(dm.cfg["save_path_aligned"])

        for (
            image_name,
            image_w0,
            image_w1,
            num_steps,
            plot_corner1,
            image_r0,
            image_r1,
            image_initial_r,
            plot_corner_r0,
            plot_corner_r1,
            plot_corner_initial,
            filepath_potentially_correct,
        ) in zip(
            image_names,
            images_w0,
            images_w1,
            batch["num_steps"],
            plot_corners1,
            images_r0,
            images_r1,
            images_initial_r,
            plot_corners_r0,
            plot_corners_r1,
            plot_corners_initial,
            batch["path_homography"],
            strict=True,
        ):
            iio.mimsave(
                image_path / "potentially_correct.gif",
                [to_pil_image(image.float()) for image in [image_w0, image_w1]],
                "GIF",
                duration=500,
                loop=0,
            )

            imgs = []
            for image, polygon in zip(
                [image_initial_r, image_r0, image_r1],
                [plot_corner_initial, plot_corner_r0, plot_corner_r1],
                strict=True,
            ):
                image_p = draw_points(image, polygon, thickness=5, radius=10)
                imgs.append(image_p)
            imgs_t = torch.stack(imgs)
            save_image(imgs_t, image_path / "potentially_correct.jpg")

            correct_alignment = input("Good?")

            if "y" in correct_alignment:
                filepath_alignment = save_path_aligned / f"{image_name}.geojson"
                save_alignment(
                    image_name=image_name,
                    polygon=plot_corner1,
                    path=filepath_alignment,
                    num_steps=int(num_steps) + 1,
                )
            else:
                Path(filepath_potentially_correct).unlink()


if __name__ == "__main__":
    main()
