from pathlib import Path
from typing import Any

import hydra
import torch
from omegaconf import DictConfig
from torchvision.io import ImageReadMode, read_image
from torchvision.transforms.functional import resize
from torchvision.utils import save_image

from plotalignment.load import get_geometry_from_reference
from plotalignment.transforms.functional import check_initial_reference_validity
from plotalignment.utils import draw_points, read_filenames_regex_pd


@hydra.main(config_path="conf", config_name="predict", version_base=None)
def main(cfg: DictConfig | dict[str, Any]) -> None:
    datapipes = cfg.datamodule.datapipe
    cfg_initial = datapipes.remote.initial
    df_initial = read_filenames_regex_pd(
        **cfg_initial.pipe, path_column_name="path_points"
    )
    df_initial = df_initial[
        df_initial["yearly_experiment_number"] == cfg.experiment_number
    ]

    cfg_images = datapipes.remote.png
    df_images = read_filenames_regex_pd(
        **cfg_images.pipe, path_column_name="path_image"
    )

    df = df_initial.merge(
        df_images, on=["yearly_experiment_number", "plot_uid", "date", "time"]
    )

    sample = df.iloc[0]

    image_size = torch.as_tensor(cfg.datamodule.cfg.image_size) // 2
    image_size_original = torch.as_tensor(cfg.datamodule.cfg.image_size_original)
    scale = image_size_original / image_size
    scale = scale[[1, 0]]

    for sample in df.to_dict(orient="records"):
        sample = get_geometry_from_reference(sample)
        if check_initial_reference_validity(sample):
            print("Valid.")
            continue
        image = read_image(sample["path_image"], mode=ImageReadMode.RGB) / 255
        image = resize(image, image_size)

        image_p = draw_points(image, sample["polygon"] / scale)
        save_image(
            image_p,
            "images/initial_alignment.jpg",
        )

        correct_alignment = input(f"{sample['plot_uid']}: Bad?")

        if "y" in correct_alignment:
            filepath_initial = f"/home/mboss/public-mnt/Evaluation/FIP/Analysis/{cfg.year}/{cfg.experiment_number}/RGB_Referencing/initial_references/{Path(sample['path_points']).stem}.geojson"
            if Path(filepath_initial).exists():
                Path(filepath_initial).unlink()
            if Path(sample["path_points"]).exists():
                Path(sample["path_points"]).unlink()


if __name__ == "__main__":
    torch.set_float32_matmul_precision("medium")

    main()
