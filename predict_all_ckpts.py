import warnings
from functools import partial
from pathlib import Path
from typing import Any

import hydra
import torch
import torch.multiprocessing
from dask.distributed import Client
from hydra.utils import instantiate
from loguru import logger
from omegaconf import DictConfig
from torch.utils.data import DataLoader
from torchvision.transforms.v2 import Compose

from plotalignment.systems.utils import (
    predict_with_homography,
    predict_with_matches,
    save_files,
)
from plotalignment.utils import consume


@hydra.main(config_path="conf", config_name="predict", version_base=None)
def main(cfg: DictConfig | dict[str, Any]) -> None:
    logger.remove(0)
    logger.add(
        f"logs/predict_{cfg['year']}_{cfg['experiment_number']}.log",
        mode="w",
        enqueue=True,
    )

    ckpt_names = [
        "aligned_highres_finetuned",
        "manual_references_highres_finetuned",
        "manual_references",
    ]

    while True:
        new_predictions = False

        for ckpt_name in ckpt_names:
            logger.info(f"Predicting with {ckpt_name}.")
            cfg.ckpt_name = ckpt_name

            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                system = instantiate(cfg["system"])
                trainer = instantiate(cfg["trainer"])

                while True:
                    dm = instantiate(cfg["datamodule"])
                    dm.prepare_data()
                    dm.setup("predict")

                    try:
                        batch = next(iter(dm.predict_dp))
                        new_predictions = True
                    except StopIteration:
                        logger.success("Finished predicting.")
                        break
                    if "mkpts0_f" in batch:
                        dp = (
                            dm.predict_dp.batch(1)
                            .collate()
                            .threadpool_map(
                                Compose(
                                    [
                                        partial(
                                            predict_with_matches,
                                            check_parameters=cfg["check_parameters"],
                                        ),
                                        partial(
                                            save_files,
                                            save_path_aligned=Path(
                                                cfg["save_path_aligned"]
                                            ),
                                            save_path_unalignable=Path(
                                                cfg["save_path_unalignable"]
                                            ),
                                            save_path_potentially_correct=Path(
                                                cfg["save_path_potentially_correct"]
                                            ),
                                        ),
                                    ]
                                ),
                                max_workers=4,
                            )
                        )

                        consume(
                            DataLoader(
                                dp,
                                batch_size=None,
                                num_workers=cfg["num_predict_workers"],
                            ),
                            progress=True,
                        )
                    elif "hom_0to1" in batch:
                        dp = (
                            dm.predict_dp.batch(1)
                            .collate()
                            .map(predict_with_homography)
                            .map(
                                partial(
                                    save_files,
                                    save_path_aligned=Path(cfg["save_path_aligned"]),
                                    save_path_unalignable=Path(
                                        cfg["save_path_unalignable"]
                                    ),
                                    save_path_potentially_correct=Path(
                                        cfg["save_path_potentially_correct"]
                                    ),
                                )
                            )
                        )

                        consume(
                            DataLoader(
                                dp,
                                batch_size=None,
                                num_workers=cfg["num_predict_workers"],
                            ),
                            progress=True,
                        )
                    else:
                        trainer.predict(system, dataloaders=dm)

        if not new_predictions:
            break


if __name__ == "__main__":
    torch.set_float32_matmul_precision("medium")

    client = Client()  # start distributed scheduler locally.
    print(client.dashboard_link)

    main()
